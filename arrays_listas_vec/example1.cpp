//vectores listas y arrays
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <list>

using namespace std;

// void show(const array<int,3> &x)
// {
//     for(array<int,3>::const_reverse_iterator i = x.rbegin(); 
//         i != x.rend(); ++i)
//     {
//         cout << *i << endl;
//     }
// }

template<typename T>
void show(const T &x)
{
    for(auto &i : x)
    {
        cout << i << endl;
    }
}

int main()
{
    vector<int> p = {3,4,6,2,6,5};
    p.push_back(13);
    // size_t len = p.size();
    // try
    // {
    //     for(auto i = 0; i < len+100000000; i++)
    //     {
    //         cout << p[i] << endl;
    //     }
    // }catch(const exception& ex)
    // {
    //     cout << ex.what() << endl;
    // }
    //show(p);
    // for(auto i = p.begin()+1; i != p.end()-1; ++i)
    // {
    //     cout << p[i] << endl;
    // }

    list<string> l = {"Tuesday","Wednesday","Thursday"};
    l.push_back("Friday");
    l.push_front("Monday");
    show(l);
    cout << "**********" << endl;
    cout << l.back() << endl;
    cout << l.front() << endl;
    l.pop_back();
    cout << l.back() << endl;
    cout << "**********" << endl;
    show(l);
    return 0;
}