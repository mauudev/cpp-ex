//vectores listas y arrays
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <functional>

using namespace std;

struct P
{   
    int n;
    P(int n):n(n){}
    ~P()
    {
        cout << "Bye" << endl;
    }
};
int main()
{
    // vector<unique_ptr<P>> x;
    // x.emplace_back(new P{10});
    // x.emplace_back(new P{15});

    // auto &q = x[1];//accede al unique_ptr
    // cout << q->n << endl;
    // cout << "*******" << endl;
    // vector<reference_wrapper<P>> rx;
    // for(auto &i : x)
    //     rx.push_back(*i);

    // for(auto &i : rx){
    //     //cout << i.get().n << endl;
    //     cout << static_cast<P&>(i).n << endl;
    // }

    vector<int(*)(int,int)> ops;
    ops.push_back([](int a,int b){return a+b;});
    ops.push_back([](int a,int b){return a-b;});
    for(auto &op : ops)
    {
        cout << op(6,4) << endl;
    }
}