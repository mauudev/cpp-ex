#include <map>
#include <unordered_set>
#include <tuple>
#include <iostream>
using namespace std;

ostream& operator<<(ostream &os, const tuple<string,string,int> &x)
{
    os << get<0>(x) << " " << get<1>(x) << " (" << get<2> (x) << ") ";
    return os;
}
int main()
{
    map<int, tuple<string,string,int>> ps;
    ps.insert(pair<int,tuple<string,string,int>> {233112, tuple<string,string,int>{"Jose","Alvarez",1980}});
    ps.insert(make_pair(222342,make_tuple("jorge","cortez",1992)));

    ps[454333] = make_tuple("Paola","Diaz",1983);

    for(auto &p : ps)
    {
        cout << "Key: " << p.first << " value: " << p.second << endl;
    }

    cout << "********************" << endl;

    // auto &q = ps[1234567];
    // cout << get<2>(q) << endl; //NO USAR ESTO USAR FIND EN SU LUGAR
    auto r = ps.find(454333);
    if(r == ps.end())
        cout << "Not found !" << endl;
    else
        cout << r->second << endl; 

    
    unordered_set<int,tuple<string,string,int>> qs;

    // for(auto &p : ps)
    //     qs.insert(p);
    // cout << "********************" << endl;
    // for(auto &q : qs)
    //     cout << q.first << " ; " << q.second << endl;

}