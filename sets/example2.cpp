
#include <iostream>
#include <tuple>
#include <unordered_set>
#include <cstring>
using namespace std;
/*
    * CONTIENEN DATOS DEL MISMO TIPO
    * LOS ELEMENTOS SE PUEDEN ITERAR
    * SE PUEDE VER SI UN ELEMENTO ESTA EN UN SET
    * ES UN ARBOL BINARIO DE BUSQUEDA
*/
struct alumno
{
    string nombre;
    int id;
};
//DECLARACION DE FUNCTORES
struct alumno_hash
{
    size_t operator()(const alumno &a)const
    {
        return 0;
    }
};

struct alumno_eq
{
    bool operator()(const alumno &a, const alumno &b) const
    {
        return tie(a.nombre, a.id) == tie(b.nombre, b.id);
    }
};
int main()
{
    unordered_set<alumno,alumno_hash,alumno_eq> ds;

    ds.insert(alumno{"Jose",3443});
    ds.insert(alumno{"Maria",34123});
    ds.insert(alumno{"Jesus",3433});

    for(auto& a:ds)
    {
        cout << a.nombre << " ; " << a.id << endl;
    }

    auto x = ds.find(alumno{"Jose",3443});
    if(x == ds.end())
        cout << "Not found" << endl;
    else 
        cout << "Found" << endl;    
}