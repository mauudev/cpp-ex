#include <iostream>
#include <string>
#include <set>
#include <tuple>

using namespace std;

/*
  set -> arbol binario de busqueda balanceado
  unordered_set -> tabla hash
*/


struct my_pair
{
  int x, y;
};


struct my_pair_comparator
{
  bool operator()(const my_pair& a, const my_pair& b) const
  {
    return tie(a.x, a.y) < tie(b.x, b.y);
  }
};

ostream& operator<<(ostream& os, const my_pair& e)
{
  os << "(" << e.x << ", " << e.y << ")";
  return os;
}

bool c2(const my_pair& a, const my_pair& b)
{
  return tie(a.y, a.x) < tie(b.y, b.x);
}

int main()
{
  // set<my_pair, my_pair_comparator> mps;
  // set<my_pair, decltype(c2)> mps {c2 };
  using ctype = bool(*)(const my_pair&, const my_pair&);
  set<my_pair, ctype> mps { c2 };
  mps.insert(my_pair{6,5});
  mps.insert(my_pair{8,2});
  mps.insert(my_pair{4,9});
  mps.insert(my_pair{6,6});

  for(auto& e : mps)
  {
    cout << e << "\n";
  }
}