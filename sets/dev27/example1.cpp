#include <iostream>
#include <string>
#include <set>

using namespace std;

/*
  set -> arbol binario de busqueda balanceado
  unordered_set -> tabla hash
*/

int main()
{
  set<string> s;
  s.insert("Java");
  s.insert("C++");
  s.insert("Rust");
  s.insert("Swift");
  s.insert("Python");
  s.insert("C#");

  for(auto i = s.begin(); i != s.end(); ++i)
  {
    cout << *i << "\n";
  }

  s.erase("Swift");
  cout << "*******************" << "\n";
  
  for(auto& x : s)
  {
    cout << x << "\n";
  }

  auto it = s.find("Python");
  if(it != s.end())
  {
    cout << "FOUND AND KILLED: " << *it << "\n";
    s.erase(it);
  }
  else cerr << "NOT FOUND\n";
}
