#include <iostream>
#include <string>
#include <unordered_set>
#include <tuple>

using namespace std;

int main()
{ 
  unordered_set<string> uns;
  uns.insert("January");
  uns.insert("February");
  uns.insert("March");
  uns.insert("April");
  uns.insert("May");

  uns.erase("May");

  for(auto& e : uns)
  {
    cout << e << "\n";
  }

  unordered_set<string>::iterator it = uns.find("April");

  if(it == uns.end())
  {
    cerr << "NOT FOUND !\n"; 
  }  
  else cout << "FOUND: " << *it << "\n";
}