#include <set>
#include <iostream>
#include <tuple>
#include <unordered_set>
using namespace std;
/*
    * CONTIENEN DATOS DEL MISMO TIPO
    * LOS ELEMENTOS SE PUEDEN ITERAR
    * SE PUEDE VER SI UN ELEMENTO ESTA EN UN SET
    * ES UN ARBOL BINARIO DE BUSQUEDA
*/
struct alumno
{
    string nombre;
    int id;
};

struct less_alumno
{
    bool operator()(const alumno &a, const alumno &b)const
    {
        return a.nombre < b.nombre;
    }
};

int main()
{
    //set<string> ds;
    //set<alumno, less_alumno> ds;
    // ds.insert("Irving");
    // ds.insert("Mauricio");
    // ds.insert("Paolo");
    // ds.insert("Marco");
    // ds.insert("Rai");

    // for(const string &e:ds)
    // {
    //     cout << e << endl;
    // }

    // cout << "****************" << endl;
    // //ESTE METODO FIND SIEMPRE USAR POR RENDIMIENTO EN LA BUSQUEDA !!!
    // set<string>::iterator it = ds.find("Mauricio");
    // if(it == ds.end())
    //     cout << "Not found !" << endl;
    // else 
    //     cout << *it << endl;

    // size_t p = ds.erase("Paolo");
    // cout << "Erased: " << p << endl;
    // cout << "****************" << endl;
    // for(const string &e:ds)
    // {
    //     cout << e << endl;
    // }
    auto f = [](auto &a, auto &b)
    {
        // if(a.id < b.id) return true;
        // if(a.id > b.id) return false;
        // return a.nombre < b.nombre;
        
        //esto hace lo de arriba
        return tie(a.id,a.nombre) < tie(b.id,b.nombre);
        
    };
    set<alumno, decltype(f)> ds{f};

    ds.insert(alumno{"Mauricio",1223});
    ds.insert(alumno{"Paolo",223});
    ds.insert(alumno{"Rai",223});
    ds.insert(alumno{"Ariel",1133});

    for(auto &a : ds)
        cout << a.nombre << " ; " << a.id << endl;


    // for(auto &a : ds)
    // {
    //     cout << a.nombre << endl;
    // }
}