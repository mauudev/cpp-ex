#include <unordered_map>
#include <tuple>
#include <memory>
#include <iostream>
#include <functional>
using namespace std;

template<typename T, typename Q, typename R>
void ins(T& m, Q q, R r)
{
    m.insert(make_pair(make_unique<Q>(q),r));
}

using up = unique_ptr<int>;
using mfunc = function<void (int)>;

void f(int n)
{
    cout << n*n << endl;
}
struct up_hash
{
    size_t operator()(const unique_ptr<int> &n) const
    {
        return *n;
    }
};

struct up_eq
{
    bool operator()(const up &a, const up &b) const
    {
        return *a == *b;
    }
};
int main()
{
    // unordered_map<up, void(*)(int), up_hash, up_eq> ms;
    // ins(ms,64,[](int p){cout << p+1 << endl;});
    // ins(ms,125,f);
    // ms[make_unique<int> (64)](12);

    unordered_map<up, mfunc, up_hash, up_eq> ms;
    ins(ms,64,[](int p){cout << p+1 << endl;});
    ins(ms,125,f);
    ms[make_unique<int> (64)](12);

    int m = 200;
    ins(ms,148,[&m](int n){
        cout << m + n << endl;
    });

    ms[make_unique<int> (148)] (22);
}