#include <iostream>
#include <cstring>

using namespace std;

class Figura {
	double area;
	string nombre;

public:
	Figura(double _area, string _nombre){
		area = _area;
		nombre = _nombre;
	}
	
	const double get_area()const {return area;}

	const string get_nombre()const {return nombre;} 

	virtual ~Figura(){}

	virtual void calcular_area()const = 0;
};

class Rectangulo : public Figura{
	double base;
	double altura;

public:
	Rectangulo(double _area, string _nombre, double base, double altura):Figura(_area,_nombre),base{base},altura{altura}{}

	void calcular_area()const override{
		_area = base * altura;
		cout << _area << endl; 
	}
};

// class Triangulo : public Figura{
// 	double base;
// 	double altura;

// public:
// 	Triangulo(const double& base, const double& altura):Figura(area{0},nombre{"Triangulo"}),base{base},altura{altura}{}

// 	void calcular_area()const override{
// 		area = (base * altura) / 2;
// 		cout << area << endl;
// 	}
// };

int main (){
	Rectangulo r{2.0,3.0};
	r.calcular_area();
}