#include <iostream>
#include <typeinfo>

using std::cout;

struct X
{
  virtual ~X(){}
};

struct Y : X
{
  int  *n;
  Y(int p):n{new int{p}}
  {

  }

  ~Y()
  {
    cout << "Bye !" << *n << "\n";
    delete n;
  }

  void show() const
  {
    cout << *n << "\n";
  }
};

void print(const X& x)
{
  
  try
  {
    auto &y = dynamic_cast<const Y&>(x);
    y.show();
  }
  catch(const std::exception& e)
  {
    std::cerr << "It is not Y" << e.what() << '\n';
  }
  
}

int main()
{
  X *x = new X();
  auto y = dynamic_cast<Y*>(x);
  if(y) y->show();
  print(*x);
  delete x;
}