#include <iostream>
using namespace std;

int main()
{
  int p = 8, q = 9;
  if(typeid(p) == typeid(q))
    cout << "Hello !\n";
  
  double r = 123.123;
  if(typeid(r) == typeid(double))
    cout << "It's a double" << "\n";
}