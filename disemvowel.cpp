#include <iostream>
#include <cstring>
using namespace std;

bool is_in_array(char s){
	char dict[] = {'a','e','i','o','u','A','E','I','O','U',0};
	size_t dict_len = strlen(dict);
	bool res = false;
	for(size_t i = 0; i < dict_len; i++){
		if(s == dict[i]) res = true;
	}
	return res;
}

char* disemvowel(const char* s){
	size_t len = strlen(s);
	auto nn = new char[len+1];
	memcpy(nn,s,len+1);
	size_t i = 0;
	size_t j = 0;
	auto res = new char[200];
	while(i < len){
		char character = nn[i];
		if(is_in_array(character)) {
			//nothing to do
		}else{
			res[j] = character;
			j++;
			} 
		i++;
	}
	res[j] = '\0';
	return res;
}
int main(){
	char* res = disemvowel("This is a test");
	cout << res << endl;
}