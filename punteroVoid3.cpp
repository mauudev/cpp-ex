#include <stdlib.h>
#include <iostream>
#include <cstdio>
using namespace std;

void a1(){
	cout << "Hello" << endl;
}
void a2(){
	cout << "Romulo" << endl;
}
void a3(){
	cout << "Alvaro" << endl;
}

typedef void(*Action)();
int main(){
	Action a[] = {a1,a2,a3};
	for(auto p : a){
		p();
	}
}