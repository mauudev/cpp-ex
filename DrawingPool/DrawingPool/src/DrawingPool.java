import java.util.ArrayList;
public class CanvasContainer {
	private ArrayList<Tool> toolContainer;
	private String name;
	private int XDimension;
	private int YDimension;
	private int totalDimension;
	
	public CanvasContainer(String name, int XDimension, int YDimension) {
		this.name = name;
		this.XDimension = XDimension;
		this.YDimension = YDimension;
		totalDimension = XDimension * YDimension;
		toolContainer = new ArrayList<Tool>();
	}
	public String getName() {return name;}
	public int getTotalDimension() {return totalDimension;}
	public ArrayList<Tool> getToolContainer(){return toolContainer;}
	public void addTool(Tool t) {
		toolContainer.add(t);
		System.out.println("New Tool added to canvas !");
	}
	public void removeTool(Tool t) {
		toolContainer.remove(t);
		System.out.println("Tool removed from canvas !");
	}
	public void showToolContainer() {
		for(Tool t: toolContainer)
			t.drawTool();
	}
};

public class ConnectionLine extends Tool {
	private Tool initialConnection, endConnection;
	
	public ConnectionLine(String name, String shape,String label,String description, int XCoordenate, int YCoordenate) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
	}

	public Tool getInitialConnection() {return initialConnection;}
	public Tool getEndConnection() {return endConnection;}
	public void setInitialConnection(Tool t) { this.initialConnection = t;}
	public void setEndConnection(Tool t) {this.endConnection = t;}
	public void removeConnection() {
		this.initialConnection = null;
		this.endConnection = null;
	}
};


public class Connector extends FlowDiagramTool{

	public Connector(String name, String shape,String label,String description, int XCoordenate, int YCoordenate) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
	}
};


public class Decision extends FlowDiagramTool{
	private ConnectionLine yesOption, noOption;
	
	public Decision(String name, String shape,String label, String description, int XCoordenate, int YCoordenate, ConnectionLine yesOption, ConnectionLine noOption) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
		this.yesOption = yesOption;
		this.noOption = noOption;
	}
	public ConnectionLine getYesOption() {return yesOption;}
	public ConnectionLine getNoOption() {return noOption;}
};


public class Event {
	
	public void dragToolToCanvas(Tool t, CanvasContainer c) {
		c.addTool(t);
	}
	public void removeToolFromCanvas(Tool t, CanvasContainer c) {
		c.removeTool(t);
	}
	public void connectTools(Tool t1, Tool t2, ConnectionLine c) {
		c.setInitialConnection(t1);
		c.setEndConnection(t2);
		System.out.println("Connection done from: "+t1.getName()+" to: "+t2.getName());
	}
	public void removeConnection(ConnectionLine c) {
		c.removeConnection();
	}
};

public class FlowDiagramTool extends Tool {

	
	public FlowDiagramTool(String name, String shape,String label, String description, int XCoordenate, int YCoordenate) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
	}
	
};

public class Process extends FlowDiagramTool{
	private ArrayList<CanvasContainer> subProcess;
	
	public Process(String name, String shape,String label, String description, int XCoordenate, int YCoordenate) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
		subProcess = new ArrayList<CanvasContainer>();
	}
	public ArrayList<CanvasContainer> getSubProcess(){return subProcess;}
	public void addSubProcess(CanvasContainer c) {subProcess.add(c);}
	public void showSubProcess() {
		for(CanvasContainer c: subProcess) {
			c.showToolContainer();
		}
	}
};

public class Terminator extends FlowDiagramTool {
	private boolean isStart,isEnd;
	
	public Terminator(String name, String shape,String label, String description, int XCoordenate, int YCoordenate,boolean isStart, boolean isEnd) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
		this.isStart = isStart;
		this.isEnd = isEnd;
	}
	public boolean isStart() {return isStart;}
	public boolean isEnd() {return isEnd;}
};

public class Tool {
	private String name;
	private String shape;
	private int XCoordenate;
	private int YCoordenate;
	private String label;
	private String description;
	
	public Tool(String name, String shape,String label, String description, int XCoordenate, int YCoordenate) {
		this.name = name;
		this.shape = shape;
		this.XCoordenate = XCoordenate;
		this.YCoordenate = YCoordenate;
		this.label = label;
		this.description = description;
	}
	public String getName() {return name;}
	public String getShape() {return shape;}
	public String getLabel() {return label;}
	public String getDescription() {return description;}
	public int getXCoordenate() {return XCoordenate;}
	public int getYCoordenate() {return YCoordenate;}
	
	public void setName(String name) {this.name = name;}
	public void setShape(String shape) {this.shape = shape;}
	public void setXCoordenate(int XC) {XCoordenate = XC;}
	public void setYCoordenate(int YC) {YCoordenate = YC;}
	public void drawTool() {
		System.out.println("Tool "+getShape()+" '"+getName()+" "+getLabel()+" "+getDescription()+"' at X: "+ XCoordenate+" and Y: "+YCoordenate+"\n");
	}
};

public class Toolbox {
	private ArrayList<Tool> flowDiagramCategory;
	private ArrayList<Tool> UMLCategory;
	private ArrayList<Tool> entityRelationCategory;
	
	public Toolbox() {
		flowDiagramCategory = new ArrayList<Tool>();
		UMLCategory = new ArrayList<Tool>();
		entityRelationCategory = new ArrayList<Tool>();
	}
	public void addFlowDiagramTool(Tool t) {
		flowDiagramCategory.add(t);
	}
	public void addUMLCategoryTool(Tool t) {
		UMLCategory.add(t);
	}
	public void addEntityRelationCategory(Tool t) {
		entityRelationCategory.add(t);
	}
	public void removeFlowDiagramTool(Tool t) {
		flowDiagramCategory.remove(t);
	}
};

public class Main {
	public static void main(String[] args) {
		Event e = new Event();
		CanvasContainer c = new CanvasContainer("Canvas1", 50, 50);
		
		Terminator t1 = new Terminator("Start", "Oval","Start", "Some description", 5, 7, true, false);
		ConnectionLine c1 = new ConnectionLine("Connection 1","Directional Connector", "Connect t1 to p1","Some description", 5, 5);
		Process p1 = new Process("Process 1", "Rectangle", "Look for lost item","Some description",7, 10);
		ConnectionLine c2 = new ConnectionLine("Connection 2","Directional Connector","Connect p1 to t1","Some description", 5, 10);
		Decision d1 = new Decision("Decision 1", "Diamond","Did you find it?", "Some description",7, 20, c1, c2);
		ConnectionLine c3 = new ConnectionLine("Connection 3","Directional Connector","Yes option d1 to p2","Some description", 5, 15);
		ConnectionLine c4 = new ConnectionLine("Connection 4","Directional Connector", "No option d1 to d2","Some description", 15, 20);
		Decision d2 = new Decision("Decision 2", "Diamond","Do you need it?", "Some description",7, 20, c1, c2);
		ConnectionLine c5 = new ConnectionLine("Connection 5","Directional Connector", "Yes option d2 to p1","Some description", 30, 15);
		ConnectionLine c6 = new ConnectionLine("Connection 6", "Directional Connector","No option d2 to p2","Some description", 25, 40);
		Terminator t2 = new Terminator("Stop looking", "Oval","Stop looking","Some description", 5, 7, false, true);

		e.dragToolToCanvas(t1, c);
		e.dragToolToCanvas(p1, c);
		e.dragToolToCanvas(c1, c);
		e.dragToolToCanvas(c2, c);
		e.dragToolToCanvas(d1, c);
		e.dragToolToCanvas(d2, c);
		e.dragToolToCanvas(c3, c);
		e.dragToolToCanvas(c4, c);
		e.dragToolToCanvas(t2, c);
		
		c1.setInitialConnection(t1);
		c1.setEndConnection(p1);

		c2.setInitialConnection(p1);
		c2.setEndConnection(d1);
		
		c3.setInitialConnection(d1);
		c3.setEndConnection(d2);
		
		c4.setInitialConnection(d1);
		c4.setEndConnection(t2);
		
		c5.setInitialConnection(d2);
		c5.setEndConnection(p1);
		
		c6.setInitialConnection(d2);
		c6.setEndConnection(t2);
		
		//HASTA AQUI SE AGREGA TODO Y SE CONECTA
		
		c.showToolContainer();
	}
};



