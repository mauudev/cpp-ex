
public class ConnectionLine extends Tool {
	private Tool initialConnection, endConnection;
	
	public ConnectionLine(String name, String shape,String label,String description, int XCoordenate, int YCoordenate) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
	}

	public Tool getInitialConnection() {return initialConnection;}
	public Tool getEndConnection() {return endConnection;}
	public void setInitialConnection(Tool t) { this.initialConnection = t;}
	public void setEndConnection(Tool t) {this.endConnection = t;}
	public void removeConnection() {
		this.initialConnection = null;
		this.endConnection = null;
	}
}
