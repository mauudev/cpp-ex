
public class Decision extends FlowDiagramTool{
	private ConnectionLine yesOption, noOption;
	
	public Decision(String name, String shape,String label, String description, int XCoordenate, int YCoordenate, ConnectionLine yesOption, ConnectionLine noOption) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
		this.yesOption = yesOption;
		this.noOption = noOption;
	}
	public ConnectionLine getYesOption() {return yesOption;}
	public ConnectionLine getNoOption() {return noOption;}
}
