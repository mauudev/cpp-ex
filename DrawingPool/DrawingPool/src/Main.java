
public class Main {
	public static void main(String[] args) {
		Event e = new Event();
		CanvasContainer c = new CanvasContainer("Canvas1", 50, 50);
		
		Terminator t1 = new Terminator("Start", "Oval","Start", "Some description", 5, 7, true, false);
		ConnectionLine c1 = new ConnectionLine("Connection 1","Directional Connector", "Connect t1 to p1","Some description", 5, 5);
		Process p1 = new Process("Process 1", "Rectangle", "Look for lost item","Some description",7, 10);
		ConnectionLine c2 = new ConnectionLine("Connection 2","Directional Connector","Connect p1 to t1","Some description", 5, 10);
		Decision d1 = new Decision("Decision 1", "Diamond","Did you find it?", "Some description",7, 20, c1, c2);
		ConnectionLine c3 = new ConnectionLine("Connection 3","Directional Connector","Yes option d1 to p2","Some description", 5, 15);
		ConnectionLine c4 = new ConnectionLine("Connection 4","Directional Connector", "No option d1 to d2","Some description", 15, 20);
		Decision d2 = new Decision("Decision 2", "Diamond","Do you need it?", "Some description",7, 20, c1, c2);
		ConnectionLine c5 = new ConnectionLine("Connection 5","Directional Connector", "Yes option d2 to p1","Some description", 30, 15);
		ConnectionLine c6 = new ConnectionLine("Connection 6", "Directional Connector","No option d2 to p2","Some description", 25, 40);
		Terminator t2 = new Terminator("Stop looking", "Oval","Stop looking","Some description", 5, 7, false, true);

		e.dragToolToCanvas(t1, c);
		e.dragToolToCanvas(p1, c);
		e.dragToolToCanvas(c1, c);
		e.dragToolToCanvas(c2, c);
		e.dragToolToCanvas(d1, c);
		e.dragToolToCanvas(d2, c);
		e.dragToolToCanvas(c3, c);
		e.dragToolToCanvas(c4, c);
		e.dragToolToCanvas(t2, c);
		
		c1.setInitialConnection(t1);
		c1.setEndConnection(p1);

		c2.setInitialConnection(p1);
		c2.setEndConnection(d1);
		
		c3.setInitialConnection(d1);
		c3.setEndConnection(d2);
		
		c4.setInitialConnection(d1);
		c4.setEndConnection(t2);
		
		c5.setInitialConnection(d2);
		c5.setEndConnection(p1);
		
		c6.setInitialConnection(d2);
		c6.setEndConnection(t2);
		
		//HASTA AQUI SE AGREGA TODO Y SE CONECTA
		
		c.showToolContainer();
	}
}
