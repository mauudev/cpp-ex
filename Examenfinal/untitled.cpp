bool test6()
{
    json_array ja;
    ja.add(true);
    ja.add("hello world"s);
    ja.add(237);
    return ja.to_string() == "[true, \"hello world\", 237]";
}