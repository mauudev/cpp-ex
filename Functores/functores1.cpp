#include <iostream>
using namespace std;
using namespace std::string_literals;

class incr
{
public:
    void operator()(int n) const
    {
        cout << (n+1) << "\n";
    }
    incr(){cout << "Hello\n";}

};

int main()
{
    incr p;
    p(16);//es un function object o functor
}