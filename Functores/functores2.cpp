#include <iostream>
#include <cstdio>
#include <string>
using namespace std;
using namespace std::string_literals;

template<typename T, typename PROC>
//void msort(const T& a, const T& b, int(*p)(T a,T b))
void msort(const T& a, const T& b, PROC p)
{
    if(p(a,b) < 0)
    {
        cout << a << "\n";
        cout << b << "\n";
    }
    else
    {
        cout << b << "\n";
        cout << a << "\n";
    }
}

template<typename T>
struct micomp
{
    bool asc;

    int operator()(const T& a, const T& b)const
    {
        if(a == b) return 0;
        if(asc)
        {
            return a < b ? -1 : 1;
        }
        return b < a ? -1 : 1;
    }
};
int cint(int a, int b)
{
    return a - b;
}

int cstr(string x, string y)
{
    return x.compare(y);
}
int main()
{
    // p(16);//es un function object o functor
    msort(8,5,cint);
    puts("********");
    msort("hola"s,"amigos"s,cstr);
    micomp<int> c1{true};
    msort(16,14,c1);
    micomp<string> c2{false};
    msort("gato"s,"perro"s,c2);
}