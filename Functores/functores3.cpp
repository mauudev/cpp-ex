#include <iostream>
#include <string>
//using namespace std;
using namespace std;

bool empieza_con_m(const string &s)
{
    return s[0] == 'm';
}

struct empieza_con
{
    char c;
    bool operator()(const string &s)const
    {
        return s[0] == c;
    }
};

template<typename T, typename PRED>
void show(const T* arr, size_t n, PRED pred)
{
    for(auto i = 0U; i < n; i++)
    {
        if(pred(arr[i]))
            cout << arr[i] << "\n";
    }
}

int main()
{
    string p[] = {"enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
    //show(p,5,empieza_con_m);

    // empieza_con ec{'e'};
    // show(p,5,ec);

    // show(p,5,[](const auto &s){//es como un closure
    //     return s.length() > 5;
    // });
    // puts("********");

    auto n = 0U;
    show(p,12,[&n](const auto &s){//&n,&p,&q
        if(s[s.length()-1] == 'e')
        {
            n++;
            return true;
        }
        return false;
    });
    cout << n << "\n";
}