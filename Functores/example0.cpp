
#include <iostream>
#include <string>
//using namespace std;
using namespace std;

/**
 *  OBJECTOS
 *  CUYA SINTAXIS PERMITE EJECUTARLOS COMO SI FUERAN FUNCTIONES
 */


struct Sumador
{
  int descuento;
  int operator()(int a, int b) const
  {
    return a + b - descuento;
  }
};

int main()
{
  Sumador s{5};
  cout << s(10,15) << "\n";
}