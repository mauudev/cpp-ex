#include <iostream>
#include <vector>
using namespace std;

std::vector<int> reverseSeq(int n) {
  std::vector<int> res;
  for(int i = n; i >= 1; i--) res.push_back(i);
  return res;
}

int main()
{
    std::vector<int> v = reverseSeq(10);
    for(int i = 0; i < v.size(); i++) cout << v.at(i)<< ", ";    
}