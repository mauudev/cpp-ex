#include <iostream>
#include <cstring>

using namespace std;


class Person
{
private:
    char* name;
public:
    Person(const char* n)
    {
        size_t len = strlen(n);
        name = new char[len+1];
        memcpy(name,n,len+1);
    }
    ~Person()
    {
        delete []name;
    }
    Person(const Person& p)
    {
        cout<<"copyctor\n";
        size_t len = strlen(p.name);
        name = new char[len+1];
        memcpy(name,p.name,len+1);
    }
    Person& operator=(const Person& p)
    {
        cout<<"copy\n";
        delete[]name;
        size_t len = strlen(p.name);
        name = new char[len+1];
        memcpy(name,p.name,len+1);
        return *this;
    }
    Person(Person&& src)
    :name{src.name}
    {
        src.name = nullptr;
    }
    void show() const
    {
        cout<<name<<'\n';
    }

};
int main()
{
    Person p{"Juan Perez"};
    Person q = p;
    p.show();
    q.show();

    Person r = move(q);
    r.show();
    q.show();
    return 0;
}