#include <iostream>
#include <string>

using namespace std;

int main() 
{
  pq<string, 4> s;
  s.enqueue(2, "hello");
  s.enqueue(3, "world");
  s.enqueue(1, "friends");
  s.enqueue(0, "today");
  s.enqueue(0, "is");
  s.enqueue(0, "Tuesday");
  s.enqueue(1, "of");
  s.enqueue(1, "C++");
}