#include <iostream>
#include <string>

using namespace std;

int sum(int a, int b)
{
  return a + b;
}

int inc(int &n)
{
  n++;
  return n;
}

string killx(string&& s)
{ 
  string p = move(s);
  return p;
}

template<typename PROC, typename ...ARGS>
auto invoke(PROC p, ARGS&& ...args)
{
  return p(forward<ARGS>(args)...);
}

int main() 
{
  auto r = invoke(sum,4,3);
  cout << r << "\n";
  int n = 8;
  invoke(inc, n);
  cout << n << "\n";
  
  //string px = "hello world";
  //string ss = invoke(killx, px);
  //cout << ss << " " << px << endl;

}