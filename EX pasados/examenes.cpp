#primer examen

int test0()
{
    dev25vm vm;
    init_vm(&vm);

    opcodes prog[] = { push, 5, push, 9,add};

    int val;
    int r = run_vm(&vm, prog, 5, &val);
    end_vm(&vm);

    return r && val == 14;
}

// int test1()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     opcodes prog[] = { push, 5, push, 0, divv, 0}; // divide by zero should return error

//     int val;
//     int r = run_vm(&vm, prog, 5, &val);
//     end_vm(&vm);

//     return r == 0;
// }

// int test2()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     opcodes prog[] = { push, 5, nop, nop, push, 4, push, 6, add, mul }; // divide by zero should return error

//     int val;
//     int r = run_vm(&vm, prog, 10, &val);
//     end_vm(&vm);

//     return r && val == 50;
// }

// int test3()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     opcodes prog[] = { push, 1, inc, inc, inc };

//     int val;
//     int r = run_vm(&vm, prog, 5, &val);
//     end_vm(&vm);

//     return r && val == 4;
// }

// int test4()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     //x0 = 10;
//     //x1 = 20;
//     //if (x0 > x1) return x0 else x1;
//     opcodes prog[] = { load0, 10, load1, 20, push0, push1, jmp_if_greater, 10, push0, ret, push1  };

//     int val;
//     int r = run_vm(&vm, prog, 11, &val);
//     end_vm(&vm);

//     return r && val == 20;
// }

// int test5()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     //int i = 0
//     //int s = 0
//     //while (i < 5)
//     // s += i
//     // return s
//     opcodes prog[] = { load0, 0, load1, 0, push0, push, 5, jmp_if_greater_or_equal, 15, push1, push0, add, pop1, jmp, 4, push1 };

//     int val;
//     int r = run_vm(&vm, prog, 16, &val);
//     end_vm(&vm);

//     return r && val == 15;
// }

// int test6()
// {
//     return test5();
// }

// int test7()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     //int n = 1
//     //for (int i = 2; i <= 6; i++)
//     //  n *= i;
//     //return n
//     opcodes prog[] = {
//         load0, 1,
//         load1, 2,
//         push1,
//         push, 6,
//         jmp_if_greater, 18,
//         push0,
//         push1,
//         mul,
//         pop0,
//         push1,
//         inc,
//         pop1,
//         jmp, 4,
//         push0
//     };

//     int val;
//     int r = run_vm(&vm, prog, 19, &val);
//     end_vm(&vm);

//     return r && val == 720;
// }

// int test8()
// {
//     return test7();
// }

// int test9()
// {
//     dev25vm vm;
//     init_vm(&vm);

//     //x0 = 1
//     //x1 = 3
//     //if (x0 == 1 && x1 == 2)
//     //  return 1000;
//     //else
//     // return x1 + 1;
//     opcodes prog[] = {
//         load0, 1,
//         load1, 3,
//         push0,
//         push, 1,
//         cmp_eq,
//         push1,
//         push, 2,
//         cmp_eq,
//         cmp_and,
//         push, 1,
//         jmp_if_not_equal, 20,
//         push, 1000,
//         ret,
//         push1,
//         inc
//     };

//     int val;
//     int r = run_vm(&vm, prog, 22, &val);
//     end_vm(&vm);

//     return r && val == 4;
// }



typedef int (*PTEST)();

int main()
{
    PTEST tests[] = { test0}; //test1, test2, test3, test4, test5, test6, test7, test8, test9 };

    int score = 0;
    for (int i = 0U; i < 1; i++)
    {
        int partial_score = tests[i]();
        printf("Test%d: %2d/10\n", i, partial_score * 10);
        score += partial_score;
    }

    printf("******\nTOTAL SCORE: %d/100\n", score * 10);

    getchar();

    return 0;
}










******************************************************
segundo examen

size_t test0()
{
    XmlDocument doc{"SecondTest"};

    char aux[100];
    *aux = 0;
    doc.Serialize(aux);
    return strcmp(aux, "<SecondTest/>") == 0;
}



/*

size_t test1()
{
    XmlDocument doc{ "Carros" };

    char aux[100]; *aux = 0;
    doc.AppendChild(new XmlElement("Carro1"));
    doc.AppendChild(new XmlElement("Carro2"));
    doc.AppendChild(new XmlElement("Carro3"));
    doc.Serialize(aux);
    return strcmp(aux, "<Carros><Carro1 /><Carro2 /><Carro3 /></Carros>") == 0;
}

size_t test2()
{
    XmlDocument doc{ "Nombre" };

    char aux[100]; *aux = 0;
    doc.AppendChild(new XmlText("Trent Reznor"));
    doc.Serialize(aux);
    return strcmp(aux, "<Nombre>Trent Reznor</Nombre>") == 0;
}

size_t test3()
{
    XmlDocument doc{ "Carros" };

    char aux[100]; *aux = 0;
    doc.AppendChild(new XmlElement("Carro1"));

    auto carro2 = new XmlElement("Carro2");
    carro2->AppendChild(new XmlText("Ferrari"));
    carro2->AppendChild(new XmlElement("Portofino"));

    doc.AppendChild(carro2);
    doc.AppendChild(new XmlElement("Carro3"));
    doc.Serialize(aux);
    return strcmp(aux, "<Carros><Carro1 /><Carro2>Ferrari<Portofino /></Carro2><Carro3 /></Carros>") == 0;
}

size_t test4()
{
    char aux[500]; *aux = 0;

    XmlDocument doc{ "Alumnos" };

    auto alumno = new XmlElement("Alumno");
    auto nombre = new XmlElement("Nombre");
    auto apellido = new XmlElement("Apellido");
    nombre->AppendChild(new XmlText("Marilyn"));
    apellido->AppendChild(new XmlText("Manson"));
    alumno->AppendChild(nombre);
    alumno->AppendChild(apellido);
    doc.AppendChild(alumno);

    alumno = new XmlElement("Alumno");
    nombre = new XmlElement("Nombre");
    apellido = new XmlElement("Apellido");
    nombre->AppendChild(new XmlText("Jean Michel"));
    apellido->AppendChild(new XmlText("Jarre"));
    alumno->AppendChild(nombre);
    alumno->AppendChild(apellido);
    alumno->AppendChild(new XmlComment("This is the father of electronica music"));
    doc.AppendChild(alumno);

    doc.Serialize(aux);

    return strcmp(aux, "<Alumnos><Alumno><Nombre>Marilyn</Nombre><Apellido>Manson</Apellido></Alumno><Alumno><Nombre>Jean Michel</Nombre><Apellido>Jarre</Apellido><!-- This is the father of electronica music --></Alumno></Alumnos>") == 0;
}

size_t test5()
{
    XmlDocument doc{ "Tests" };

    char aux[100]; *aux = 0;

    auto e1 = new XmlElement("Test1");
    auto e2 = new XmlElement("Test2");
    doc.AppendChild(e1);
    doc.AppendChild(e2);

    e1->AppendChild(new XmlText("Say \"Hello\""));
    e2->AppendChild(new XmlText("C & C Music Factory <<The music>>"));

    doc.Serialize(aux);
    return strcmp(aux, "<Tests><Test1>Say &quot;Hello&quot;</Test1><Test2>C &amp: C Music Factory &lt;&lt;The music&gt;&gt;</Test2><Tests>") == 0;
}

size_t test6()
{
    return test5();
}

size_t test7()
{
    char aux[500]; *aux = 0;

    XmlDocument doc{ "Cantantes" };

    auto cantante = new XmlElement("Cantante");
    cantante->SetAttribute("nombre", "Taylor");
    cantante->SetAttribute("apellido", "Swift");
    doc.AppendChild(cantante);

    cantante = new XmlElement("Cantante");
    cantante->SetAttribute("nombre", "Justin");
    cantante->SetAttribute("apellido", "Timberlake");
    cantante->AppendChild(new XmlComment("Comment; elements"));
    doc.AppendChild(cantante);

    doc.Serialize(aux);

    return strcmp(aux, "<Cantantes><Cantante nombre=\"Taylor\" apellido=\"Swift\" /><Cantante nombre=\"Justin\" apellido=\"Timberlake\"><!-- Comment; elements --></Cantante></Cantantes>") == 0;
}


size_t test8()
{
    char aux[500]; *aux = 0;

    XmlDocument doc{ "Alumnos" };

    auto alumno = new XmlElement("Alumno");
    auto nombre = new XmlElement("Nombre");
    auto apellido = new XmlElement("Apellido");
    nombre->AppendChild(new XmlText("Marilyn"));
    apellido->AppendChild(new XmlText("Manson"));
    alumno->AppendChild(nombre);
    alumno->AppendChild(apellido);
    doc.AppendChild(alumno);

    alumno = new XmlElement("Alumno");
    nombre = new XmlElement("Nombre");
    apellido = new XmlElement("Apellido");
    nombre->AppendChild(new XmlText("Jean Michel"));
    apellido->AppendChild(new XmlText("Jarre"));
    alumno->AppendChild(nombre);
    alumno->AppendChild(apellido);
    alumno->AppendChild(new XmlComment("This is the father of electronica music"));
    doc.AppendChild(alumno);

    doc.Serialize(aux, true);

    puts(aux);

    return strcmp(aux,
        "<Alumnos>\n"
        "    <Alumno>\n"
        "        <Nombre>\n"
        "            Marilyn\n"
        "        </Nombre>\n"
        "        <Apellido>\n"
        "            Manson\n"
        "        </Apellido>\n"
        "    </Alumno>\n"
        "    <Alumno>\n"
        "        <Nombre>\n"
        "            Jean Michel\n"
        "        </Nombre>\n"
        "        <Apellido>\n"
        "            Jarre\n"
        "        </Apellido>\n"
        "        <!-- This is the father of electronica music -->\n"
        "    </Alumno>\n"
        "</Alumnos>\n") == 0;
}

size_t test9()
{
        return test8();
}

*/
using test_type = size_t(*)();

auto main() -> int
{
        test_type ts[] = {
                test0/*, test1, test2, test3, test4,
                test5, test6, test7, test8, test9*/ };

        auto score = 0U, i = 0U;
        for (auto& t : ts)
        {
                auto scoret = t();
                printf("Test %zu: %zu / 1\n", i++, scoret);
                score += scoret;
        }
        puts("-----------------");
        printf("SCORE: %zu / 10\n", score);
        getchar();
        return 0;
}


***********************************************************
final

const string text = "Doom’s success lay not only with its addicting gameplay and technical innovations, such as its introduction of a portable game engine, but also in its innovative digital distribution. Note the prominent 'shareware' advertisement on the top front of the game packaging. This groundbreaking game earned a spot in the inaugural class of the World Video Game Hall of Fame.";

bool test0()
{
    text_store ts;
    ts.load_from_string(text);

    return ts.get_word_count() == 60;
}


bool test1()
{
    text_store ts;
    ts.load_from_string(text);

    return ts.get_word_count_by_first_letter('d') == 3;
}

/*
bool test2()
{
    text_store ts;
    ts.load_from_string("hello hello bye hello bye");

    size_t hello_count = 0, bye_count = 0;
    ts.iterate_word_occurrences([&](const word_occurrence& c)
    {
        if (c.word == "hello")
            hello_count = c.count;

        if (c.word == "bye")
            bye_count = c.count;
    });

    return hello_count == 3 && bye_count == 2;
}

bool test3()
{
    text_store ts;
    ts.load_from_string("binary world world");

    stringstream ss;
    ts.iterate_word_occurrences([&](const word_occurrence& c)
    {
        ss << c << "; ";
    });

    return ss.str() == "binary (1); world (2); ";
}

bool test4()
{
    text_store ts;
    ts.load_from_string(text);

    const word_info& wloc = ts.get_word_info("Its");

    stringstream ss;
    ss << wloc;

    return ss.str() == "its (33, 91, 147)";
}

bool test5()
{
    try
    {
        text_store ts;
        ts.load_from_string(text);

        const word_info& wloc = ts.get_word_info("brisa mistica");

        cout << wloc << "\n";
        return false;
    }
    catch (const exception& ex)
    {
        return string{ ex.what() } == "'brisa mistica' not found";
    }
}

bool test6()
{
    text_store ts;
    ts.load_from_string(text);

    size_t count = 0, total_count = 0;
    ts.iterate_all_words_by_first_letter('t', [&](const word_info& w)
    {
        auto& word = w.word;
        total_count++;
        if (word == "technical" || word == "the" || word == "this" || word == "top")
            count++;
    });

    return count == 4 && total_count == 4;
}

bool test7()
{
    text_store ts;
    ts.load_from_string(text);

    return ts.get_most_occurring_word().word == "the";
}

bool test8()
{
    text_store ts;
    ts.load_from_file("vulgata.txt");

    return ts.get_word_count() == 702484 &&
           ts.get_word_count_by_first_letter('a') == 4260 &&
           ts.get_most_occurring_word().word == "et" &&
           ts.get_most_occurring_word().count == 51614 &&
           ts.get_word_info("maria").locations.back() == 3736761;
}

bool test9()
{
    return test8();
}

*/

int main()
{
    size_t score = 0;

    auto t0 = clock();

    score += test0();
    score += test1();
   /* score += test2();
    score += test3();
    score += test4();
    score += test5();
    score += test6();
    score += test7();
    score += test8();
    score += test9();*/

    auto t1 = clock();

    cout << "FINAL SCORE: " << score << " / 10\n";
    cout << "EXECUTION TIME: " << ((t1 - t0) / 1000) << "\n ";

    getchar();

    return 0;
}