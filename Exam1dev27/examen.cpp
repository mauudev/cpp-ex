#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class object {

public:
    object(){}
    virtual ~object(){}
    virtual bool operator==(const object& obj)const
    {
        return false;
    }
    virtual std::string get_classname() const
    {
        return "object";
    }
    virtual std::string to_string() { return "";}
};

class array_list
{
    size_t len;
    size_t memory_size;
    object** objs;
    
    public:
    
    array_list():len{0},memory_size{4}, objs{new object*[4]}
    {

    }

    ~array_list()
    {
        for(size_t i = 0; i < len; i++)
        {
            delete objs[i];
        }
        delete [] objs;
    }

    array_list& operator=(const array_list& src)
    {
        if(this == &src)
        {
            return *this;
        }
        this->~array_list();
        len = src.len;
        for(size_t i = 0; i < len; i++)
        {
            objs[i] = src.objs[i];
        }
        cout << "Retornando copia\n";
        return *this;
    }

    void add(object* obj)
    {
        if(len < memory_size)
        {
            objs[len] = obj;
            len ++;
            return ;
        }
        size_t aux_size;
        aux_size = 2 * memory_size;
        object** aux_array = new object*[aux_size];
        for(size_t i = 0; i < len; i++)
        {
            aux_array[i] = objs[i];
        }
        delete [] objs;
        objs = aux_array;
        objs[len ++] = obj;
        memory_size = aux_size;
    }

    object& get(size_t index)
    {
        return *objs[index];
    }

    size_t get_count()
    {
        return len;
    }

    std::string get_classname() const 
    {
        return "array_list";
    }
};

class JsonNumber : public object
{
  size_t value;
  public:
  JsonNumber(size_t val):value{val}
  {

  }

  std::string to_string() override
  {
    return std::to_string(value);
  }
};

class JsonString : public object
{
  std::string value;
  public:
  JsonString(const std::string& val):value{val}
  {

  }

  std::string add_quote()
  {
    return "\"";
  }

  std::string to_string() override
  {
    return add_quote() + value + add_quote();
  }
};

class JsonObject : public object
{
  std::string open_brace;
  std::string close_brace;
  array_list keys;
  array_list values;

  public:
  JsonObject():open_brace{"{"},close_brace{"}"}{}
  
  std::string get_open_brace()
  {
    return open_brace;
  }

  std::string get_close_brace()
  {
    return close_brace;
  }

  void add(const std::string &key, const std::string &value)
  {
    JsonString *jskey = new JsonString(key);
    JsonString *jsval = new JsonString(value);
    keys.add(jskey);
    values.add(jsval);
  }
  void add(const std::string &key, size_t value)
  {
    JsonString *jskey = new JsonString(key);
    JsonNumber *jsval = new JsonNumber(value);
    keys.add(jskey);
    values.add(jsval);
  }

  std::string to_string() override
  {
    std::string res = get_open_brace();
    size_t keys_len = keys.get_count();
    size_t values_len = values.get_count();
    if(keys_len > 0 && values_len > 0 && keys_len == values_len)
    {
      res += " ";
      for(size_t i = 0; i < keys_len - 1; i ++ )
      {
        res += keys.get(i).to_string();
        res += ": ";
        res += values.get(i).to_string();
        res += ", ";
      }
      res += keys.get(keys_len - 1).to_string();
      res += ": ";
      res += values.get(values_len - 1).to_string();
      res += " ";
    }
    res += get_close_brace();
    return res;
  }
};

class JsonArray : public object
{
  std::string open_bracket;
  std::string close_bracket;
  array_list elements;

  public:
  JsonArray():open_bracket{"["}, close_bracket{"]"}
  {

  }
  
  std::string get_open_bracket()
  {
    return open_bracket;
  }

  std::string get_close_bracket()
  {
    return close_bracket;
  }


  void add(size_t value)
  {
    JsonNumber *jn = new JsonNumber(value);
    elements.add(jn);
  }

  void add(const std::string& value)
  {
    JsonString *js = new JsonString(value);
    elements.add(js);
  }

  void add(JsonObject& jo)
  {
    object *obj = dynamic_cast<object *>(&jo);
    elements.add(obj);
  }

  std::string to_string() override
  {
    std::string res = get_open_bracket();
    size_t len = elements.get_count();
    if(len > 0)
    {
      for(size_t i = 0; i < len - 1; i++)
      {
        res += elements.get(i).to_string();
        res += ", ";
      }
      res += elements.get(len-1).to_string();
    }
    res += get_close_bracket();
    return res;
  }
};

bool test0()
{
    JsonNumber jn { 3141 };
    return jn.to_string() == "3141";
}

bool test1()
{
    JsonObject jo;
    return jo.to_string() == "{}";
}

bool test2()
{
    JsonString js { "hello" };
    return js.to_string() == "\"hello\"";
}

bool test3()
{
    JsonArray ja;
    return ja.to_string() == "[]";
}

bool test4()
{
    JsonArray ja;
    ja.add(125);
    ja.add("hello");
    ja.add("friends");
    return ja.to_string() == "[125, \"hello\", \"friends\"]";
}

bool test5()
{
    JsonObject jo;
    jo.add("firstName", "John");
    jo.add("lastName", "Smith");
    jo.add("birthYear", 1977);
    return jo.to_string() == "{ \"firstName\": \"John\", \"lastName\": \"Smith\", \"birthYear\": 1977 }";
}

bool test6()
{
    JsonArray ja;
    ja.add("hello");

    JsonObject jo;
    jo.add("firstName", "John");
    jo.add("lastName", "Smith");
    jo.add("birthYear", 1977);

    ja.add(jo);

    ja.add(1492);

    return ja.to_string() == "[\"hello\", { \"firstName\": \"John\", \"lastName\": \"Smith\", \"birthYear\": 1977 }, 1492]";
}

// bool test7()
// {
//     JsonObject jo;

//     jo.add("nombre", "jugo de frutas");

//     JsonArray ingredients;
//     ingredients.add("leche");
//     ingredients.add("frutilla");
//     ingredients.add("manzana");
//     ingredients.add("azucar");

//     jo.add("ingredientes", ingredients);
//     return jo.to_string() == "{ \"nombre\": \"jugo de frutas\", \"ingredientes\": [ \"leche\", \"frutilla\", \"manzana\", \"azucar\" ] }";
// }

// bool test8()
// {
//     JsonArray ja;
//     ja.add("hello");

//     JsonObject jo;
//     jo.add("firstName", "John");
//     jo.add("lastName", "Smith");
//     jo.add("birthYear", 1977);

//     ja.add(jo);

//     ja.add(1492);

//     return ja.to_string(true) ==
//     "[\n"
//     "    \"hello\",\n"
//     "    {\n"
//     "        \"firstName\": \"John\",\n"
//     "        \"lastName\": \"Smith\",\n"
//     "        \"birthYear\": 1977\n"
//     "    },\n"
//     "    1492\n"
//     "]";
// }

// bool test9()
// {
//     JsonObject jo;

//     jo.add("nombre", "jugo de frutas");

//     JsonArray ingredients;
//     ingredients.add("leche");
//     ingredients.add("frutilla");
//     ingredients.add("manzana");
//     ingredients.add("azucar");

//     jo.add("ingredientes", ingredients);
//     return jo.to_string(true, 2) ==
//     "{\n"
//     "  \"nombre\": \"jugo de frutas\",\n"
//     "  \"ingredientes\": [\n"
//     "    \"leche\",\n"
//     "    \"frutilla\",\n"
//     "    \"manzana\",\n"
//     "    \"azucar\"\n"
//     "  ]\n"
//     "}";
// }

int main()
{
    size_t score = 0;
    score += test0();
    score += test1();
    score += test2();
    score += test3();
    score += test4();
    score += test5();
    score += test6();
    // score += test7();
    // score += test8();
    // score += test9();

    cout << "Score: " << score << " / 10\n";
}