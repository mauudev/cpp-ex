#include<iostream>
#include<cstring>
#include "DoubleStringLinkedList.cpp"
using namespace std;
//las cadenas en C solo son arreglos de caracteres

size_t mylen(const char* s){//de esta manera nos aseguramos que lo q entra como parametro NO SERA MODIFICADO
	size_t n = 0;
	while(s[n] != '\0') n++;
	return n;
}

char* mycopy(char* s1, const char* s2){
	size_t lim = mylen(s2);
	while(*s2 != '\0') {
		*s1++ = *s2++;
	}
	*s1 = 0;
	return s1;
}

char* split_with_index(char* out, const char* in, int index){
	//int lim = mylen(in);
	int i = 0;
	while(i < index){
		out[i] = in[i];
		i++;
	}
	out[i] = 0;
	return out;
}

char* myconcat(char* dst, const char* src){
	size_t lim = mylen(dst);
	mycopy(dst+lim, src);
	return dst;
}

size_t count_words(const char* s){//hola hoy es lunes = 4
	size_t res = 0;
	size_t aux = 0;
	size_t tam = mylen(s);
	if(tam > 0){
		while(*s != '\0'){
			if(*s == ' ' && aux > 0){
				res++;
				*s++;
				aux = 0;
			}else{
				*s++; 
				aux++;
			} 
		}
		if(aux > 0) res++;
	}
	return res;
}

size_t count_ocurrences(const char* s, const char* f){//hola hoy es lunes | hola
	size_t t1 = strlen(s);
	size_t i = 0;
	size_t j = 0;
	size_t k = 0;
	size_t res = 0;
	auto nn = new char[t1+1];
	memcpy(nn,s,t1+1);
	char tok[200];
	while(i < t1){
		if(nn[i] == ' '){
			while(j < i){
				tok[k] = nn[j];
				k++;
				j++;
			}
			tok[k] = '\0';
			k = 0;
			j = i+1;
			if(strcmp(tok,f) == 0) res++;
			*tok = 0;
		}
		i++;
	}
	return res;
}

void count_ocurrences_phrases(const char* s, const char* f){
	size_t t1 = strlen(f);
	size_t i = 0;
	size_t j = 0;
	size_t k = 0;
	size_t res = 0;
	auto nn = new char[t1+1];
	memcpy(nn,f,t1+1);
	char tok[200];
	while(i < t1){
		if(nn[i] == ' ' || nn[i] == '\0'){
			while(j < i){
				tok[k] = nn[j];
				k++;
				j++;
			}
			tok[k] = '\0';
			k = 0;
			j = i+1;
			cout << tok << endl;
			res = count_ocurrences(s,tok);
			cout << "La palabra: " << tok << " se repite: " << res << " veces." << endl;
			*tok = 0;
			res = 0;
		}
		i++;
	}
	tok[i] = '\0';
}
int main(){
	// char s1[] = {'h','e','l','l','o',0};
	// //cout << s1 << "\n";
	// char s2[] = "hola";
	// //cout << s2 << "\n";
	// char s3[6];
	// s3[0] = 'a';
	// s3[1] = 'l';
	// s3[2] = 'o';
	// s3[3] = 'h';
	// s3[4] = 'a';
	// s3[5] = 's';
	// //cout << s3 << "\n";

	// const char* s4 = " hola mundo";//al declarar como puntero se convierte en una constante y no se puede modificar su contenido
	// //cout << s4 << "\n";
	// //s4[0] = "r";//esto no se puede hacer sale error en tiempo de compilacion
	// //cout << s4 << "\n";

	// size_t p = mylen(s4);
	// //cout << p << "\n";

	// char s5[mylen(s4)];
	// mycopy(s5,s4);
	// //cout << s5 << "\n";

	// myconcat(s2,s4);
	// //cout << s2 << "\n";

	// char str[] = "hola hoy es lunes";
	// size_t count = count_words(str);
	// //cout << "hola hoy es lunes tiene " << count << " palabras" << '\n';

	// char x[4];
	// char c[1];
	// c[0] = ' ';
	// //mycopyuntilchar(x,str,c);
	// //cout << x << "\n";
	char str1[] = "holas hoy es lunes siii hoy holas";
	char str2[] = "holas";
	//count_ocurrences_phrases(str1,str2);
	size_t cont = count_ocurrences(str1, str2);
	cout << "La palabra: " << str2 << " se repite: " << cont << " veces." << endl;
	
}
	
