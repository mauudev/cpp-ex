#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>

using namespace std;

// This is a database in memory that can store objects.
// You can have several 'tables' (lists of objects) assible by their name.
// Any "table" can have unlimited number of objects.
// You will be able to create tables, add objects to the tables, count number of tables and objects, filter, sort and find objects given an size_t ID.
// The memory_db will be in charge of releasing the objects stored into it.
//
// Restrictions:
// * Do not use anything we did not see in class. You can use std::string.
// * Memory leaks will cost you 10 points.
// * Program crashing will cost you 50 points.
// * Program not compiling will cost you 100% of your exam.

//**************** CLASS OBJECT *******************
//**************** CLASS MEMORY DB ****************
class object {

public:
    object(){}
    virtual ~object(){}
    virtual std::string to_string() const { return "";}
    virtual bool operator==(const object& obj)const
    {
        return false;
    }
    virtual std::string get_classname() const
    {
        return "object";
    }
    virtual size_t get_count() const
    {
        return 0;   
    }
    virtual size_t get_id()
    {
        return 0;
    }
};

class array_list : public object 
{
public:
    size_t len;
    size_t memory_size;
    object** objs;
    std::string name;
    
    array_list(const std::string& n):len{0},memory_size{4}, objs{new object*[4]}
    {
        name = n;
    }

    array_list():len{0}, memory_size{4}, objs{new object*[4]}
    {
        name = "no name";
    }
    ~array_list()
    {
        for(size_t i = 0; i < len; i++)
        {
            delete objs[i];
        }
        delete [] objs;
    }

    array_list& operator=(const array_list& src)
    {
        if(this == &src)
        {
            return *this;
        }
        this->~array_list();
        len = src.len;
        name = src.name;
        for(size_t i = 0; i < len; i++)
        {
            objs[i] = src.objs[i];
        }
        cout << "Retornando copia\n";
        return *this;
    }

    std::string get_array_list_name()
    {
        return name;
    }

    void add(object* obj)
    {
        if(len < memory_size)
        {
            objs[len] = obj;
            len ++;
            return ;
        }
        size_t aux_size;
        aux_size = 2 * memory_size;
        object** aux_array = new object*[aux_size];
        for(size_t i = 0; i < len; i++)
        {
            aux_array[i] = objs[i];
        }
        delete [] objs;
        objs = aux_array;
        objs[len ++] = obj;
        memory_size = aux_size;
    }

    object& get(size_t index)
    {
        return *objs[index];
    }

    size_t get_count() const override
    {
        return len;
    }

    std::string get_classname() const override 
    {
        return "array_list";
    }
};

class student : public object
{
    size_t id;
    std::string name;
    std::string last_name;

public:
    student(size_t i, const std::string &n, const std::string &l ):id{i}, name{n}, last_name{l}{}
    ~student(){}

    size_t get_id() override
    {
        return id;
    }

    std::string get_name() const 
    {
        return name;
    }

    std::string get_last_name() const
    {
        return last_name;
    }

    std::string get_classname() const override
    {
        return "student";
    }
};

class course : public object
{
    size_t id;
    std::string course_name;

public:
    course(size_t id, const std::string &course_name):id{id}, course_name{course_name}
    {

    }

    ~course()
    {

    }

    size_t get_id()
    {
        return id;
    }

    std::string get_classname() const override 
    {
        return "course";
    }

    std::string get_course_name() const
    {
        return course_name;
    }
};

class memory_db {
    array_list tables;

public:
    memory_db(){}
    ~memory_db(){}

    void create_table(const std::string& name)
    {
        array_list *new_table = new array_list(name);
        tables.add(new_table);
    }

    size_t get_table_count()
    {
        return tables.get_count();
    }

    const array_list get_tables()
    {
        return tables;
    }

    array_list& get_table_by_name(const std::string &table_name)
    {
        array_list *another = new array_list("void");
        for(size_t i = 0; i < tables.get_count(); i++)
        {
            array_list& table = static_cast<array_list &>(tables.get(i));
            // cout << "Target: " << table_name << " GOT: " <<table.get_array_list_name() <<"\n";
            if(table.get_array_list_name() == table_name){
                return table;
            }
        }
        return *another;
    }

    array_list* get_table_by_name_pointer(const std::string &table_name)
    {
        for(size_t i = 0; i < tables.get_count(); i++)
        {
            array_list* table = static_cast<array_list *>(tables.objs[i]);
            if(table->get_array_list_name() == table_name){
                cout << "Encontrado ! \n";
                return table;
            }
        }
        array_list *another = new array_list("void");
        return another;
    }

    bool add_object(const std::string &table_name, object* obj)
    {
        array_list &table_target = get_table_by_name(table_name);
        if(table_target.get_array_list_name() != "void")
        {
            table_target.add(obj);
            return true;
        }
        return false;   
    }

    size_t get_object_count(const std::string &table_name)
    {
        array_list &table_target = get_table_by_name(table_name);
        return table_target.get_count();
    }

    object* get_st_by_id(size_t id)
    {
        array_list &st_array = get_table_by_name("students");
        for(size_t i = 0; i < st_array.get_count(); i++)
        {
            student &st = static_cast<student &>(st_array.get(i));
            if(st.get_id() == id) return &st_array.get(i);
        }
        return nullptr;
    }

    size_t swap_number(const std::string &name)
    {
        if(name == "student") return 1;
        if(name == "course") return 2;
        return 0;
    }
    object* find(const std::string &obj_name, size_t id) 
    {
        size_t number = swap_number(obj_name);
        switch(number)
        {
            case 1:
                 return get_st_by_id(id);
                 break;
        }
        return nullptr;
    }

    array_list& get_all(const std::string &table_name)
    {
        return *get_table_by_name_pointer(table_name);
    }
};


//**************** TESTS **************************

// bool test0()
// {
//      memory_db db;

//      db.create_table("students");
//      db.create_table("courses");

//      return db.get_table_count() == 2;
// }

// bool test1()
// {
//      memory_db db;

//      db.create_table("students");
//      db.create_table("courses");

//      bool p = db.add_object("students", new student(123, "juan", "perez"));
//      p = db.add_object("students", new student(125, "gael", "garcia"));

//      return p && db.get_object_count("students") == 2 && db.get_object_count("courses") == 0;
// }

// bool test2()
// {
//      memory_db db;

//      db.create_table("students");
//      bool r = db.add_object("courses", new course(1, "devint26"));
//      return r == false; // false because 'courses' table does not exist in this db object
// }

// bool test3()
// {
//      memory_db db;

//      db.create_table("students");
//      db.add_object("students", new student(123, "juan", "perez"));
//      db.add_object("students", new student(125, "gael", "garcia"));

//      auto st = (student*) db.find("student", 125);
//      return st != nullptr && st->get_last_name() == "garcia";
// }

bool test4()
{
     memory_db db;

     db.create_table("students");
     db.create_table("courses");
     db.add_object("students", new student(123, "juan", "perez"));
     db.add_object("students", new student(125, "gael", "garcia"));
     db.add_object("courses", new course(1, "math"));
     db.add_object("students", new student(128, "luis", "miguel"));
     db.add_object("courses", new course(2, "physics"));
     db.add_object("students", new student(130, "marco", "peredo"));
     db.add_object("courses", new course(3, "chemistry"));

     auto list = db.get_all("students");
     cout << "TAMANO: " << list.get_count() << "\n";
    //  cout << "NOMBRE: " << ((student&) list.get(3)).get_name() << " " << ((student&)list.get(3)).get_last_name() << "\n";
    //  return list.get_count() == 4; // because "friends" table does not exist in this memory_db
    return true;
}


// bool test5()
// {
//      memory_db db;

//      db.create_table("students");
//      db.create_table("courses");
//      db.add_object("students", new student(123, "juan", "perez"));
//      db.add_object("students", new student(125, "gael", "garcia"));
//      db.add_object("courses", new course(1, "math"));
//      db.add_object("students", new student(128, "luis", "miguel"));
//      db.add_object("courses", new course(2, "physics"));
//      db.add_object("students", new student(130, "marco", "peredo"));
//      db.add_object("courses", new course(3, "chemistry"));

//      auto list = db.get_all("courses");
//      cout << list.get_count() << "\n";
//     //  return list.get_count() == 3 && list.get(0).get_id() == 1 && list.get(1).get_id() == 2 && list.get(2).get_id() == 3;
// }

// bool test6()
// {
//      memory_db db;

//      db.create_table("students");
//      db.add_object("students", new student(123, "juan", "perez"));
//      db.add_object("students", new student(125, "gael", "garcia"));
//      db.add_object("students", new student(128, "luis", "miguel"));
//      db.add_object("students", new student(130, "marco", "peredo"));

//      auto list = db.select_from("students", "student_starts_with_pe");

//      return list.get_count() == 2 &&
//                 ((student&)list.get(0)).get_last_name() == "perez" &&
//                 ((student&)list.get(1)).get_last_name() == "peredo";
// }

// bool test7()
// {
//      return test6();
// }


// bool test8()
// {
//      memory_db db;

//      db.create_table("students");
//      db.add_object("students", new student(1, "jean michel", "jarre"));
//      db.add_object("students", new student(1, "armin", "van buuren"));
//      db.add_object("students", new student(1, "vi", "clarke"));
//      db.add_object("students", new student(1, "chris", "lowe"));

//      auto list = db.order_by("students", compare_by_first_name);

//      return list.get_count() == 4 &&
//               ((student&)list.get(0)).get_first_name() == "armin" &&
//               ((student&)list.get(1)).get_first_name() == "chris" &&
//               ((student&)list.get(2)).get_first_name() == "jean michel" &&
//               ((student&)list.get(3)).get_first_name() == "vi";
// }

// bool test9()
// {
//      return test8();
// }


int main()
{
     bool r[10];

    //  r[0] = test0();
    //  r[1] = test1();
    //  r[2] = test2();
    //  r[3] = test3();
     r[4] = test4();
    //  r[5] = test5();
    //  r[6] = test6();
    //  r[7] = test7();
    //  r[8] = test8();
    //  r[9] = test9();

     auto score = 0;

     for (int i = 0; i < 10; i++)
     {
           int sc = r[i] * 10;
           cout << "TEST " << i << ": Result: " << sc << "\n";
           score += sc;
     }

     puts("**************");
     cout << "TOTAL SCORE: " << score << "\n";

     getchar();
     return 0;
}