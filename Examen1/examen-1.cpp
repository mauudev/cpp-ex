#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>

using namespace std;

// This is a database in memory that can store objects.
// You can have several 'tables' (lists of objects) accessible by their name.
// Any "table" can have unlimited number of objects.
// You will be able to create tables, add objects to the tables, count number of tables and objects, filter, sort and find objects given an size_t ID.
// The memory_db will be in charge of releasing the objects stored into it.
//
// Restrictions:
// * Do not use anything we did not see in class. You can use std::string.
// * Memory leaks will cost you 10 points.
// * Program crashing will cost you 50 points.
// * Program not compiling will cost you 100% of your exam.

//**************** CLASS OBJECT *******************
class lib_object{
     
     
public:

lib_object()
{
     
}
virtual ~lib_object()
{
}
virtual std::string to_string() const
{
     return "x";
}
virtual bool operator ==(const lib_object& obj) const
{
     return false;
}
virtual std::string get_class_name()const
{
     return "lib_object";
}

virtual size_t get_count() const{return 0;};
};
//**************** CLASS OBJECT *******************

//**************** CLASS ARRAY LIST ***************
class lib_array_list : public lib_object
{
     size_t len;
     size_t sizememory;
     lib_object** objs;//es un array que cada tipo es de lib_object
     std::string name;
     
     public:
     
     
lib_array_list():len{0},sizememory{4}, objs{new lib_object*[4]}
{
     name = "noname";
}


lib_array_list(std::string name):len{0},sizememory{4}, objs{new lib_object*[4]},name{name}
{
}

~lib_array_list()
{
     for(size_t i = 0; i < len; i++){
          delete objs[i];
     }
     delete[] objs;
     //puts("bye");
}    

std::string get_array_list_name()
{
     return name;   
}


void add(lib_object* obj)
{
     if(len<sizememory)
     {
          objs[len] = obj;
          len++;
     }else{
          size_t aux;
          aux = 2*sizememory;
          lib_object** array_aux = new lib_object*[aux];
          for(size_t i = 0; i < len; i++)
          {
               array_aux[i] = objs[i];
          }         
          delete []objs;
          objs = array_aux;
          objs[len++] = obj;
          sizememory = aux;
     }         
}

int find(const lib_object& obj) const
{
     for(size_t i=0; i<len; i++)
     {
          if(obj == *objs[i])
          {
               return i;
          }
     }
     return -1;
}


lib_object& get(size_t pos){
     
     return *(objs[pos]);
}

size_t length()const{
     return len;
}

size_t get_count()const{
     return len;
}

void decrease_length()
{
     len--;
}

std::string get_class_name()const override{
     return "lib_array_list";
}

};
//**************** CLASS ARRAY LIST ***************

//**************** CLASS STUDENT ******************
class student : public lib_object{
     size_t id;
     std::string name;
     std::string last_name;

public:

     student(size_t id, std::string name, std::string last_name):id{id},name{name},last_name{last_name}{}

     ~student(){}

     size_t get_id(){return id;}
     std::string get_name(){return name;}

     std::string get_last_name(){return last_name;}

     std::string get_class_name()const override{return "student";}

     std::string to_string()const override {
          return "Id: "+std::to_string(id)+" Name: "+std::string(name)+" Last name: "+std::string(last_name);
     }

};
//**************** CLASS STUDENT ******************


//**************** CLASS COURSE *******************
class course : public lib_object{
     size_t id;
     std::string description;
public:
     course(size_t id, std::string description):id{id},description{description}{}

     ~course(){}

     size_t get_id(){return id;}

     std::string get_description(){return description;}

     std::string get_class_name()const override{
          return "course";
     }

     std::string to_string()const override{
          return "Id: "+std::to_string(id)+" Description: "+std::string(description);
     }
};
//**************** CLASS COURSE *******************
//**************** CLASS MEMORY DB ****************
class memory_db {
private:
     lib_array_list db_list;
     size_t len;
public:
     
memory_db():len{0}{
}

~memory_db(){}

void create_table(std::string name) {
     lib_array_list* list_pointer = get_array_list_by_name_pointer(name);
     lib_array_list* db1 = new lib_array_list(name);
     db_list.add(db1);
     len++;
}

lib_array_list& get_db_list(){return db_list;}

size_t get_table_count(){
     return len;
}

lib_array_list* get_array_list_by_name_pointer(std::string name){      
     for(size_t i = 0; i < len; i++){
          lib_object& obj = db_list.get(i);
          lib_array_list& r = (lib_array_list&)obj;
          lib_array_list* s = &r;
          std::string n = s->get_array_list_name();
          if(n == name) return s;
     }
     return nullptr;  
}

lib_array_list& get_array_list_by_name_ref(std::string name){      
     for(size_t i = 0; i < len; i++){
          lib_object& obj = db_list.get(i);
          lib_array_list& r = (lib_array_list&)obj;
          std::string n = r.get_array_list_name();
          if(n == name) return r;
     }
}

bool add_object(std::string table, lib_object* object){
     std::string class_name = object->get_class_name();

     if(class_name == "student" && table == "students"){
          lib_array_list* student_list = get_array_list_by_name_pointer("students"); 
          if(student_list){
               size_t len1 = student_list->length();
               student_list->add(object);
               size_t len2 = student_list->length();
               if(len2 > len1) return true;
          }
          
     }
     if(class_name == "course" && table == "courses"){
          lib_array_list* courses_list = get_array_list_by_name_pointer("courses"); 
          if(courses_list){
               size_t len1 = courses_list->length();
               courses_list->add(object);
               size_t len2 = courses_list->length();
               if(len2 > len1) return true;
          }
     }
     return false;
}

size_t get_object_count(std::string table){
     lib_array_list* t = get_array_list_by_name_pointer(table);
     if(t) return t->length();
     return -1;
}

lib_object* find(std::string table, size_t id){
     if(table == "student"){
          lib_array_list* student_list = get_array_list_by_name_pointer("students");
          for(size_t i = 0; i < student_list->length(); i++){
               lib_object& obj = student_list->get(i);
               lib_object* s = &obj;
               student* st = (student*)s;
               if(st->get_id() == id) return s; 
          }
     }
     return nullptr;
}

// bool is_in_array_list(std::satring table){
//      lib_array_list* list = get_array_list_by_name_pointer(table);

// }_

lib_array_list& get_all(std::string table){
     lib_array_list* list = get_array_list_by_name_pointer(table);
     if(list != nullptr) {
          //cout << list->get_count() << endl;
          lib_array_list* copy_list = new lib_array_list(table);
          for(size_t i = 0; i < list->get_count()-1; i++){
               cout << list[i].to_string() << endl;
               //copy_list[i] = list[i];
          }
          //cout << copy_list->get_count() << endl;
          lib_array_list* empty_list = new lib_array_list();
          return *empty_list;
     }
     else {
          lib_array_list* empty_list = new lib_array_list();
          return *empty_list;
     }
}

};
//**************** CLASS MEMORY DB ****************

//**************** TESTS **************************

/*bool test0()
{
     memory_db db;

     db.create_table("students");
     db.create_table("courses");

     return db.get_table_count() == 2;
}

bool test1()
{
     memory_db db;

     db.create_table("students");
     db.create_table("courses");

     // //******* LA CLAVE DEL CASTEO !!!!!! *******************
     // lib_object& obj = db.get_array_list_by_name_pointer("studentss");
     // lib_array_list& r = (lib_array_list&)obj;
     // cout << r.get_array_list_name() << endl;
     // //******* LA CLAVE DEL CASTEO !!!!!! *******************

     //******* LA CLAVE DEL CASTEO !!!!!! *******************
     //lib_object* obj = db.get_array_list_by_name_pointer("students");
     //lib_array_list* r = (lib_array_list*)obj;
     //******* LA CLAVE DEL CASTEO !!!!!! *******************

     bool p = db.add_object("students", new student(123, "juan", "perez"));
     p = db.add_object("students", new student(125, "gael", "garcia"));

     return p && db.get_object_count("students") == 2 && db.get_object_count("courses") == 0;
}

bool test2()
{
     memory_db db;

     db.create_table("students");
     bool r = db.add_object("courses", new course(1, "devint26"));
     return r == false; // false because 'courses' table does not exist in this db object
}

bool test3()
{
     memory_db db;

     db.create_table("students");
     db.add_object("students", new student(123, "juan", "perez"));
     db.add_object("students", new student(125, "gael", "garcia"));

     auto st = (student*) db.find("student", 125);

     return st != nullptr && st->get_last_name() == "garcia";
}*/

// bool test4()
// {
//      memory_db db;

//      db.create_table("students");
//      db.create_table("courses");
//      db.add_object("students", new student(123, "juan", "perez"));
//      db.add_object("students", new student(125, "gael", "garcia"));
//      db.add_object("courses", new course(1, "math"));
//      db.add_object("students", new student(128, "luis", "miguel"));
//      db.add_object("courses", new course(2, "physics"));
//      db.add_object("students", new student(130, "marco", "peredo"));
//      db.add_object("courses", new course(3, "chemistry"));

//      auto list = db.get_all("students");

//      cout << list.get_count() << endl;
//      //return list.get_count() == 0; // because "friends" table does not exist in this memory_db
// }

/*
bool test5()
{
     memory_db db;

     db.create_table("students");
     db.create_table("courses");
     db.add_object("students", new student(123, "juan", "perez"));
     db.add_object("students", new student(125, "gael", "garcia"));
     db.add_object("courses", new course(1, "math"));
     db.add_object("students", new student(128, "luis", "miguel"));
     db.add_object("courses", new course(2, "physics"));
     db.add_object("students", new student(130, "marco", "peredo"));
     db.add_object("courses", new course(3, "chemistry"));

     auto list = db.get_all("courses");

     return list.get_count() == 3 && list.get(0).get_id() == 1 && list.get(1).get_id() == 2 && list.get(2).get_id() == 3;
}

bool test6()
{
     memory_db db;

     db.create_table("students");
     db.add_object("students", new student(123, "juan", "perez"));
     db.add_object("students", new student(125, "gael", "garcia"));
     db.add_object("students", new student(128, "luis", "miguel"));
     db.add_object("students", new student(130, "marco", "peredo"));

     auto list = db.select_from("students", student_starts_with_pe);

     return list.get_count() == 2 &&
                ((student&)list.get(0)).get_last_name() == "perez" &&
                ((student&)list.get(1)).get_last_name() == "peredo";
}

bool test7()
{
     return test6();
}


bool test8()
{
     memory_db db;

     db.create_table("students");
     db.add_object("students", new student(1, "jean michel", "jarre"));
     db.add_object("students", new student(1, "armin", "van buuren"));
     db.add_object("students", new student(1, "vince", "clarke"));
     db.add_object("students", new student(1, "chris", "lowe"));

     auto list = db.order_by("students", compare_by_first_name);

     return list.get_count() == 4 &&
              ((student&)list.get(0)).get_first_name() == "armin" &&
              ((student&)list.get(1)).get_first_name() == "chris" &&
              ((student&)list.get(2)).get_first_name() == "jean michel" &&
              ((student&)list.get(3)).get_first_name() == "vince";
}

bool test9()
{
     return test8();
}*/


int main()
{
     //test4();
     /*bool r[10];

     r[0] = test0();
     r[1] = test1();
     r[2] = test2();
     r[3] = test3();
     // // r[4] = test4();
     // // r[5] = test5();
     // // r[6] = test6();
     // // r[7] = test7();
     // // r[8] = test8();
     // // r[9] = test9();

     auto score = 0;

     for (int i = 0; i < 10; i++)
     {
           int sc = r[i] * 10;
           cout << "TEST " << i << ": Result: " << sc << "\n";
           score += sc;
     }

     puts("**************");
     cout << "TOTAL SCORE: " << score << "\n";

     getchar();
     return 0;*/
}