#include<cstring>
#include<iostream>
using namespace std;
constexpr size_t MAXSTR=16;//para cadenas pequenas

class Str{
	char* s;
	char sso[MAXSTR];
	size_t len;	

public:
	Str(const char* s = ""):len{strlen(s)}{
		if(len < MAXSTR)
			memcpy(sso,s,len+1);
		else{
			this->s = new char[len+1];
			memcpy(this->s,s,len+1);
		}
	}

	Str(const Str& src):len{src.len}{
		if(len < MAXSTR){
			memcpy(sso,src.sso,len+1);
			return ;
		}
		s = new char[len+1];
		memcpy(s,src.s,len+1);
	}

	Str& operator=(const Str& src){
		if(this == &src) return *this;
		this->~Str();
		if(len < MAXSTR)
			memcpy(sso,src.sso,len+1);
		else{
			s = new char[len+1];
			memcpy(s,src.s,len+1);
		}
	}

	bool operator==(const Str& src)const{
		if(this == &src) return true;
		if(len != src.len) return false;
		return memcmp(data(), src.data(), len) == 0;
	}

	bool operator==(const char* s)const{
		return strcmp(data(),s) == 0;
	}

	Str operator+(const Str& src)const{
		size_t nlen = len + src.len;
		Str aux;
		if(nlen < MAXSTR){
			memcpy(aux.sso,sso,len);
			memcpy(aux.sso+len,src.sso,src.len+1);
		}else{
			aux.s = new char[nlen+1];
			memcpy(aux.s,data(),len);
			memcpy(aux.s+len,src.data(),src.len+1);
		}
		aux.len = nlen;
		return aux;
	}

	Str operator+=(const Str& src){
		auto nlen = len+src.len;
		if(nlen < MAXSTR){
			memcpy(sso+len,src.sso,src.len+1);
		}else{
			char* p = new char[nlen+1];
			memcpy(p,data(),len);
			memcpy(p+len, src.data(),src.len+1);
			if(len >= MAXSTR) this->~Str(); 
			this->s = p;
		}
		len = nlen;
		return *this;
	}

	Str substr(size_t index, size_t size)const{// mariajuana / 5,9 = juana
		if(index > size) return "";
		Str n;
		if(size < MAXSTR){
			char* p = new char[size+1];
			char* q = new char[index+1];
			memcpy(p,s,len+1);
			size_t i = index;
			while(i < size){
				q[i] = p[i];
				i++;
			}
			q[i] = 0;
			memcpy(n.s,q,index+1);
		}
		return n;
		
	}

	~Str(){//siempre publico
		if(len >= MAXSTR)
		delete [] s;
		cout << "Bye" << "\n";
	}

	const char* data() const{
		return len < MAXSTR ? sso : s;
	}
	void show()const{
		cout << data() << "\n";
	}
};

int main(){
	/*Str x;
	Str y{"hello world !"};
	Str z{"carlos de jesus rodriguez"};
	cout << y.data() << "\n";
	Str a = y;
	//Str b = z	;
	cout << a.data() << "\n";*/
	Str s = "hello";
	Str w = "world !";

	w = s;
	w.show();
	s.show();
	/*if(w == "hello"){
		cout << "son iguales" << "\n";
	}else cout << "son diferentes" << "\n";*/
	/*Str h = s+" "+w;
	h.show();

	Str x{"devint"};
	x += "26";
	x += "fundacion-jala";
	x.show();*/
	//Str x = "MariaJuana";
	//x.substr(6,9).show();
}