//clases abstractas
//ctring usa char*
//y string maneja la clase string
#include <string>
#include <iostream>


using namespace std;
class Employee
{
	string name;
	string role;
public:
	Employee(const string& name,const string& role):name{name},role{role}{}//se pone & para evitar una copia
	const string& get_name() const
	{
		return name;
	}
	const string& get_role() const
	{
		return role;
	}
	//este es un metodo virtual puro
	virtual void show_job()const =0;//asi se declara un metodo abstracto es un metodo que esta declarado pero no implementado y asi toda la clase se hace abstracta
	virtual ~Employee()//si uso un virtual el destructor tambien tiene que ser virtual
	{

	}
};

class Janitor:public Employee
	{
	public:
	Janitor(const string &n):Employee(n,"Janitor")
	{	
	}
	void show_job()const override
	{
		cout<<"limpiar\n";
	}
};

class Boss:public Employee
{
public:
	Boss(const string& n):Employee{n,"Boss"}
	{}
	void show_job()const override
	{
		cout<<"ordenar\n";
	}
};
void print(const Employee& e)//en las clases abstractas siempre con (const Employee& )
{
	cout<<e.get_name()<<"\n";
	cout<<e.get_role()<<"\n";
	e.show_job();
}

int main()
{
	//Employee e{"juan perez","lead"};//por el echo de que esta clase es virtual no se puede realizar esto a menos que se implemente el virtual en las clases hijas
	Boss s{"juan perez"};
	s.show_job();
	print(s);
}
// desde constructores nunca llamar a metodos virtuales
// las interfaces son clases abstractas donde todos los metodos son virtuales puros