#include<iostream>
#include<cstring>

using namespace std;

class P{
	int x;

public:
	P(int x):x{x}{}

	virtual ~P(){}//destructor virtual siempre en la clase padre

	virtual void show()const{//agregando virtual agrega polimorfismo a las clases hijas
		cout << x << endl;
	}

	
protected:
	size_t get_x()const{
		return x;
	}
};

class Q:public P{ // si queiremos especificar que es la ultima clase de la herencia y ya no se deben heredar mas clases, 
	//se pone class Q:final P
	int y;

public:
	Q(int x, int y):P{x},y{y}{

	}

	void show2()const{
		P::show();
		cout << y << endl;
	}
	void show()const override{
		cout << get_x() << ";" << y << endl;
	}
};

int main(){
	/*P p{16};
	p.show();

	Q q{8,14};
	q.show();
	q.show2();*/

	P* pp = new P{25};
	pp->show();

	Q* pq = new Q{100,105};
	pq->show();

	P* qq = new Q{36,666};
	qq->show();//llama al metodo de show de P
}