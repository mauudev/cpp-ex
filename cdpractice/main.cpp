#include <iostream>
#include "line.h"
#include "rectangle.h"
#include "circle.h"
#include "text_renderer.h"
#include "photoshop.h"
#include <memory>
#include <string>

using namespace std;


void draw_menu()
{
    cout<<"Menu\n";
    cout<<"1. Add shape\n";
    cout<<"2. Move shape\n";
    cout<<"3. Resize shape\n";
    cout<<"4. Delete shape\n";
    cout<<"Quit\n";
}
template <typename T>
void process_add(T &ps)
{
    int op;
    cout << "1. line\n";
    cout << "2. circle\n";
    cout << "3. Rectangle\n";
    cin>>op;
    switch(op)
    {
        case 1: ps.add_shape(make_unique<line>());
                break;
        case 2: ps.add_shape(make_unique<circle>());
                break;
        case 3: ps.add_shape(make_unique<rectangle>());
                break;
    } 
}
template <typename T>
void process_move(photoshop<T> &ps)
{

}

template <typename T>
void process_resize(photoshop<T> &ps)
{

}

template <typename T>
void process_delete(photoshop<T> &ps)
{

}

template<typename T>
void process_option(int op, photoshop<T> &ps)
{
    switch(op)
    {
        case 1: process_add(ps);
                break;
        case 2: process_move(ps);
                break;
        case 3: process_resize(ps);
                break;
        case 4: process_delete(ps);
                break;
        case 5: break;
        default: throw -1;
    }
}

int main()
{
    photoshop <text_renderer<80,25>> ps;
    int op = -1;
    while (op != 5)
    {
        ps.show_image();
        draw_menu();
        cin >> op;
        process_option(op, ps);
        cout<<ps.get()<<"\n";
    }

}