#ifndef SHAPE_H
#define SHAPE_H
#include <string>

class shape
{
    std::string shape_name;
    int m_axisX;
    int m_axisY;
    int m_width;
    int m_height;
public:
    shape(std::string shape_name):shape_name{shape_name},
    m_axisX{0},m_axisY{0},m_width{15},m_height{15}
    {

    }
    ~shape(){}
};

#endif