#ifndef PHOTOSHOP_H
#define PHOTOSHOP_H
#include "shape.h"
#include <vector>
#include <memory>
using namespace std;

template<typename R>
class photoshop
{
    vector <unique_ptr<shape>> m_vShapes;
public:
    photoshop()
    {

    }
    template <typename TYPESHAPE>
    void add_shape(TYPESHAPE && new_shape)
    {
        m_vShapes.push_back(forward<TYPESHAPE>(new_shape));
    }
    void show_image()
    {
        // for(auto &s : m_vShapes)
        // {
            
        // }
    }    
    
    size_t get()
    {
        return m_vShapes.size();
    }
};

#endif