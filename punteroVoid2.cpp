#include <stdlib.h>
#include <iostream>
using namespace std;

/*void a (void(*p)(), void(*q)()){ //puede ser asi de forma rustica o usar typedef
	p();
	q();
}*/

typedef void(*FUNC)();
void a(FUNC p, FUNC q){
	p();
	q();
}
void b(){
	cout << "Hi" << endl;
}

void c(){
	cout << "Bye" << endl;
}

int main(){
	a(b,c);
}
