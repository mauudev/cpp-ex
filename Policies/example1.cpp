
#include <iostream>
#include <string>
using namespace std;
//comportamientos que se inyecta a una cclase template
//se especifican al momento de instanciar una clase temaplate
// se le  pasa como un parametro en el templaate

struct PerroPolicy
{
    void hablar() const
    {
        cout << "Gau Gau\n";
    }
};

struct VacaPolicy
{
    void hablar() const
    {
        cout << "Mu Mu\n";
    }
};


template <typename T>
struct Animal
{
    T t;
    void hablar() const
    {
        t.hablar();
    }
};
using Perro = Animal<PerroPolicy>;
using Vaca = Animal<VacaPolicy>;

template<typename P>
void hablar(const Animal<P>& p)
{
    p.hablar();
}

int main()
{
    Perro p;
    Vaca v;

    hablar(p);
    hablar(v);

    return 0;
}
