#ifndef MMAP2_H
#define MMAP2_H

#include <iostream>
#include <string>

using namespace std;

template<typename T>
struct default_comparator
{
    bool operator()(const T& a,const T& b)const
    {
        return a<b;
    }
    
};

template<
    typename K, 
    typename V, 
    typename COMPARATOR = default_comparator<K>>
class mmap2
{
    struct node
    {
        K key;
        V value;
        node* left;
        node* right;
    };
    node* root;
    COMPARATOR cmp;
    
    size_t size;
    public:
    mmap2()
    :root{nullptr},size{0}
    {
    }
    ~mmap2()
    {
        node* aux=root;
        iterate_destroy(root);
    }
    void iterate_destroy(node* nodo_actual)
    {
        if (nodo_actual == nullptr)
        {
            return;
        }
        iterate_destroy(nodo_actual->left);
        iterate_destroy(nodo_actual->right);
        delete nodo_actual;
        
        
    }
    
    V& insert_node(node* &nodo_actual, const K& key,const V& value)
    {
        if (nodo_actual == nullptr)
        {
            nodo_actual = new node{key, value, nullptr, nullptr};
            size++;
            return nodo_actual->value;
        }
        
        if(cmp(key, nodo_actual->key))
        {
            return insert_node(nodo_actual->left,key,value);
        }
        else if(cmp(nodo_actual->key, key))
        {
            return insert_node(nodo_actual->right,key,value);
        }
        else
        {
            throw 2;
        }
    }
    
    V& insert(const K& key,const V& value)
    {
         return insert_node(root, key, value);
    }
    size_t get_size()const
    {
        return size;
    }

    template<typename PROC>
    void iterate(PROC proc)
    {
        iterate_deep(proc,root);
    }

    template<typename PROC>
    void iterate_deep(PROC proc, node* nodo_actual)
    {
        if(nodo_actual == nullptr)
        {
            return ;
        }
        iterate_deep(proc, nodo_actual->left);
        proc(nodo_actual->key, nodo_actual->value);//funcion lambda
        iterate_deep(proc, nodo_actual->right);
    }

    const K& get_maximum_key() const
    {
        node* aux = root;
        while(aux != nullptr)
        {
            if(aux->right == nullptr)
                return aux->key;
            aux = aux->right;
        }
        throw 1;
    }

    const K& get_minimun_key() const
    {
        node* aux = root;
        while(aux != nullptr)
        {
            if(aux->left == nullptr)
                return aux->key;
            aux = aux->left;
        }
        throw 1;
    }

    const V& find(const node* current_node, const K& key) const
    {
        if(current_node == nullptr)
        {
            throw value_not_found<K>{key};
        }
        if(cmp(key, current_node->key))
            return find(current_node->left, key);
        if(cmp(current_node->right, key))
            return find(current_node->right, key);
        return current_node->value;
    }

    const V& operator[](const K& key) const
    {
        return find(root,key);
    }
#endif