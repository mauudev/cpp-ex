#include <iostream>
#include <cstring>
#include <cstdio>
#include <string>
#include <cstdlib>
using namespace std;
class lib_object{
    
public:
    lib_object(){}
    virtual ~lib_object(){}
    virtual string to_string() const
    {
        return "to_string";
    }
    virtual bool operator ==(const lib_object&) const
    {
        return true;
    }
    virtual string get_class_name()const
    {
        return "lib_object";
    }
};

class lib_array_list : public lib_object
{
    lib_object** array;
    size_t count;
    size_t size_memory;
public:
    lib_array_list()
    :array{new lib_object* [10]}, count{0},size_memory{10}
    {}
    ~lib_array_list()
    {}
    void add (lib_object* obj)
    {
        if (count<size_memory)
        {
            array[count]=obj;
            count++;
        }
        else
        {
            size_memory=size_memory*2;
            lib_object** aux_array = new lib_object*[size_memory];
            for (size_t i = 0; i < count; ++i)
            {
                aux_array[i]=array[i];
            }
            count++;
            aux_array[count]=obj;
            delete []array;
            array=aux_array;

        }

    }
    size_t length()
    {
        return count;
    }
    lib_object& get(size_t i)
    {
        return *array[i];
    }
    void show()const
    {
        for (size_t i = 0; i < count; ++i)
        {
            cout<<array[i]->to_string()<<"\n";
        }
    }
    int find(lib_object& objf)
    {
        for (size_t i = 0; i < count; ++i)
        {
            if (array[i]->to_string()==objf.to_string())
            {
                return i;
            }
        }
        return -1;
    }
    
};
class lib_string : public lib_object
{
    char* words;
public:
    lib_string(const char* aux)
    {
        int len = strlen(aux);
        words= new char[len+1];
        memcpy(words,aux,len+1);
    }
    ~lib_string(){}
    string to_string() const override
    {
        string aux=(string)words;
        return aux;
    }
    
};
class lib_integer : public lib_object
{
    int value;
public:
    lib_integer(int a)
    :value{a}
    {}
    ~lib_integer(){}
    string to_string() const override
    {
        char *aux= new char [sizeof(int)];
        sprintf(aux, "%d", value);
        string aux2=(string) aux;
        delete[]aux;
        return aux2;
    }

    
};
class lib_double : public lib_object
{
    double value;
public:
    lib_double(double a)
    :value{a}
    {}
    ~lib_double(){}
    string to_string() const override
    {
        char *aux= new char [sizeof(double)];
        sprintf(aux, "%f", value);
        string aux2=(string) aux;
        delete[]aux;
        return aux2;
    }

    
};


class lib_rawstring :public lib_object
{
    char* value;
public:
    lib_rawstring(const char* word)
    {
        int len = strlen(word);
        value= new char[len+1];
        memcpy(value,word,len+1);
    }
    ~lib_rawstring(){}

    string to_string() const override
    {
        string aux=(string)value;
        return aux;
    }
    
};

class lib_queue: public lib_object
{
    lib_array_list queue;
public:
    lib_queue()
    {}
    ~lib_queue()
    {}
    void push( lib_object* co)
    {
        queue.add(co);
    }
    lib_object& pop()
    {
        return (queue.get(0));
    }
    void show()
    {

    }
};

int main()
{
    // lib_array_list objs;
    // objs.add(new lib_string("Hello Friend"));
    // objs.add(new lib_string("World Cruel"));
    // objs.add(new lib_integer(25));
    // objs.add(new lib_string("Yeah! its "));
    // objs.add(new lib_string("AAAA"));
    // objs.add(new lib_integer(60));
    // objs.add(new lib_double(3.14156926539));
    // objs.show();
    
    // auto len = objs.length();
    // cout << "Tamano de la lista: " << len << endl;
    // for (auto i = 0U; i < len; i++)
    // {
    //     cout << objs.get(i).to_string().data() << "\n";
    // }

    // lib_double p { 3.14156926539};
    // lib_string p1{"25"};
    // lib_integer p2{0x41414141};
    // lib_rawstring p3 { "Hello" };
    
    // auto pos = objs.find(p);
    // auto pos1 = objs.find(p1);
    // auto pos2 = objs.find(p2);
    // auto pos3 = objs.find(p3);
    
    // cout << "Position: " << pos << "\n";
    // cout << "Position: " << pos1 << "\n";
    // cout << "Position: " << pos2 << "\n";
    // cout << "Position: " << pos3 << "\n";

    lib_queue c;
    c.push(new lib_integer(31));
    c.push(new lib_integer(11));
    c.push(new lib_integer(42));
    c.push(new lib_integer(23));

    c.show();
    lib_object a = c.pop();
    cout << "aqui pop " << a.to_string().data() << "\n";

    c.show();


    return 0;
    
    
    /*for (int i = 0; i < 1000000000; i++)
    {
        if (i % 10000000 == 0)
            cout << i << "\n";
        
        test2();
    }*/
    
    // test2();
    return 0;
}