int myRound(double n){
	int resultado = 0;
	int parte_entera = (int)n;
	double parte_decimal = (n - parte_entera)*10;
	double parte_decimal_copia = parte_decimal;
	while(parte_decimal_copia >= 10){
		parte_decimal_copia = parte_decimal_copia/10;
	}
	if(parte_decimal_copia >= 5) resultado = parte_entera + 1;
	else resultado = parte_entera;
	return resultado;
}