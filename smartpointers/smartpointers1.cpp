#include <iostream>
#include <string>
#include <memory>
#include <cstdio>

using namespace std;

/*
SMART POINTERS
* variable en el stack
* implementa el operator ->
* implementa alguna politica de manejo de memoria
* sirve para hacer delete de pointers usando el stack

#include <memory>

unique_ptr
shared_ptr
weak_ptr
*/

template<typename T>
class wrapper
{
    T* item;
public:
    wrapper(T* item):item{item}
    {

    }

    wrapper(const wrapper<T>&) = delete;//prohibiendo el constructor copia
    wrapper(wrapper<T> && src):item(src.item)
    {
        src.item = nullptr;
        cout << "moved" << endl;
    }
    ~wrapper(){delete item;}
    T* get()
    {
        return item;
    }

    T* operator->()
    {
        return item;
    }
};

struct A
{
    int n;
    A(int n):n{n}{}
    ~A(){cout << "Bye" << endl;}

    void print() const
    {
        cout << n << endl;
    }
};

struct closer
{
    void operator()(FILE *f)
    {
        fclose(f);
        cout << "Bye file" << endl;
    }
};

class point
{
    // A* x; ERROR 
    // A* y;
    unique_ptr<A> x;
    unique_ptr<A> y;

public:
    point(int x, int y):x{new A{x}},y{new A{y}}
    {
        s();// si no declaramos como uniqueptr los obj creados con new no se eliminan despues de ejecutar s() 
    }
    ~point()
    {
        // delete x;
        // delete y;
    }

    void print()const
    {
        x->print();
        y->print();
    }

    void s()
    {
        throw nullptr;
    }
};

int main()
{
    // wrapper<A> w{new A{155}}; //cuando termine el proceso se elimina A gracias a smart pointers
    // w.get()->print();
    // w->print();
    // wrapper<A> b = move(w);
    // b->print();

    //unique_ptr<A> c{new A{2018}};
    // auto c = make_unique<A>(2018);
    // c.get()->print();
    // unique_ptr<A> d = move(c);
    // d->print();
    // if(c == nullptr)
    //     cout << "NULL" << endl;
    // (*d).print();
    // d.reset();
    // cout << "September" << endl;

    // unique_ptr<FILE,closer> ar{fopen("abc.txt","w")};
    // fputs("hello ",ar.get());
    // fputs("world",ar.get());
    try{
        point p {40,20};
        p.print();
    }
    catch(...)
    {
        cout << "Error !" << endl;
    }
}