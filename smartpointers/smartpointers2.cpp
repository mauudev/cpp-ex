#include <iostream>
#include <string>
#include <memory>
#include <cstdio>

using namespace std;

/*
SMART POINTERS
* variable en el stack
* implementa el operator ->
* implementa alguna politica de manejo de memoria
* sirve para hacer delete de pointers usando el stack

#include <memory>

unique_ptr
shared_ptr
weak_ptr
*/

template<typename T>
class wrapper
{
    T* item;
public:
    wrapper(T* item):item{item}
    {

    }

    wrapper(const wrapper<T>&) = delete;
    wrapper(wrapper<T> && src):item(src.item)
    {
        src.item = nullptr;
        cout << "moved" << endl;
    }
    ~wrapper(){delete item;}
    T* get()
    {
        return item;
    }

    T* operator->()
    {
        return item;
    }
};

struct A
{
    int n;
    A(int n):n{n}{}
    ~A(){cout << "Bye" << endl;}

    void print() const
    {
        cout << n << endl;
    }
};

struct closer
{
    void operator()(FILE *f)
    {
        fclose(f);
        cout << "Bye file" << endl;
    }
};

class point
{
    // A* x; ERROR 
    // A* y;
    unique_ptr<A> x;
    unique_ptr<A> y;

public:
    point(int x, int y):x{new A{x}},y{new A{y}}
    {
        s();// si no declaramos como uniqueptr los obj creados con new no se eliminan despues de ejecutar s() 
    }
    ~point()
    {
        // delete x;
        // delete y;
    }

    void print()const
    {
        x->print();
        y->print();
    }

    void s()
    {
        throw nullptr;
    }
};

void p(const A& x) //para evitar copia innecesaria referencia const y por buena practica nunca poner de parametro unique, shared pointer etc
{
    x.print();
}
int main()
{
    shared_ptr<A> a {new A{15}};
    auto b = make_shared<A>(18);
    auto c = make_shared<A>(40);
    auto d = make_shared<A>(100);
    auto e = a;
    auto f = b;
    b = make_shared<A>(668);
    f = make_shared<A>(360);
    auto g = c;
    p(*a); p(*b); p(*c); p(*d); p(*e); p(*f); p(*g);
}