#include <iostream>
#include <string>
#include <memory>
#include <cstdio>

using namespace std;

struct mama_de_marco;

struct marco
{
    shared_ptr<mama_de_marco> mdm;

    ~marco(){cout << "No te vayas mama !" << endl;}
    void hi(){cout << "Hola mama !" << endl;}
};

struct mama_de_marco
{
    weak_ptr<marco> hijo; //siempre usar weak ptr si se usan shared ptr con relacion circular

    ~mama_de_marco(){cout << "Adios marco !"<< endl;}
};

int main()
{
    auto m = make_shared<marco>();
    auto mdm = make_shared<mama_de_marco>();

    m->mdm = mdm;
    mdm->hijo = m;

    m->hi();

    mdm->hijo.lock()->hi();//lock bloquea el sharedptr para q no se elimine desde otra parte
}