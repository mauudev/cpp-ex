#include <memory>
#include <iostream>
using namespace std;
struct A
{
    int n;
    A(int n):n{n}{}
    ~A(){}
    void print()
    {
        puts("entra a clase A");
        cout<<n
        <<"\n";
    }
};
template <typename T>
class wrapper
{
    T* item;
public:
    wrapper(T* p_item)
    :item{p_item}
    {
        puts("entro aki");
    }
    T* get()
    {
        puts(":v");
        return item;
    }
    T* operator->()
    {
        puts("operador sobrecargado");
        return item;
    }

    wrapper(const wrapper<T> &src) = delete;//ssirve para no permitir dos punteros a un mismo obj
    wrapper(wrapper&& value)
    :item{value.item}
    {
        cout<<"const move\n";
        value.item= nullptr;
    }

};   

class point
{
    int a;
    int b;
public:
    point(int a, int b)
    :a{a}, b{b}
    {
        throw 1;
    }
    ~point()
    {

    }

    void print()
    {
        cout<<a<<" "<<b<<"\n";
    }   
    
};
int main()
{
    // wrapper<A> w{new A{155}}; //cuando termine el proceso se elimina A gracias a smart pointers
    // w.get()->print();
    // w->print();
    // wrapper<A> b = move(w);
    // b->print();


    //unique_ptr<A> c{new A{2018}};
    unique_ptr<A> c = make_unique<A>(2018);
    c.get()->print();//get() es metodo de unique_ptr que devuelve un puntero de A
    unique_ptr<A> d = move(c);
    d->print();
  
    if(c == nullptr)
        cout << "NULL" << endl;
    (*d).print();
    d.reset();
    if(d == nullptr)
        cout << "NULL d" << endl;

    // unique_ptr<FILE,closer> ar{fopen("abc.txt","w")};
    // // fputs("hello ",ar.get());
    // // fputs("world",ar.get());
    try{
        point p {40,20};
        p.print();
    }
    catch(...)
    {
        cout << "Error !" << endl;
    }
}