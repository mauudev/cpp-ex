#include <iostream>
#include <string>
//using namespace std;
using namespace std;

string join(const string &, const string &w)
{
    return w;
}

template<typename ...args>
string join(const string &sep, const string &m, const args &...ag)
{
    return m + sep + join(sep,ag...);
}

int main()
{
    string p = join(",","Juan","Jose","Maria","Claudia");
    cout << p << endl;
}