#include <iostream>
#include <string>
//using namespace std;
using namespace std;

template<typename T>
T sumar(const T &x, const T &y)
{
    return x + y;
}

template<typename T, typename ...ARGS>
T sumar(const T &x, const ARGS &...args)
{
    return x + sumar(args...); 
} 

template<typename...ARGS>//args es un conjunto de tipos sirve para simular varios parametros (PARAMETER PACK)
void sum(const ARGS&...args)//se llama PACK EXPANSION
{
    cout << sumar(args...) << endl;//se llama PACK EXPANSION
}

int main()
{
    sum(6,8);
    sum(6.14,8.95);
    sum(6,9,2);
    sum(3.14,2.64,7.44);
    sum(5,3,6,7,4,3,2);
}

