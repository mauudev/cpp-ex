#include <iostream>
#include <string>
//using namespace std;
using namespace std;

void show()
{}

template<typename T>
void show_s(const T &t)
{
    cout << t << endl;

}
template<typename T, typename...ARGS>
void show_s(const T& t, const ARGS... args)
{
    cout << t << endl;
    show_s(args...);
}

template<typename...ARGS>
void show(const ARGS... args)
{
    show_s(args...);
}

int main()
{
    show(6,4,true,"hello",3.1416);
    //show("hola","mundo",26,"bye");
}