//especializacion con clases
#include <cstdio>
#include<iostream>
#include<string>

using namespace std;

template<typename T>
struct A
{
	T n;
	void show()const
	{
		cout<<n<<"\n";
	}
};
template<typename T>
struct A<T *>
{
	T *n;
	void print()const
	{
		cout<<*n<<"\n";
	}
	~A(){delete n;}
};
struct X
{
	int p=8;
	X()
	{
		cout<<"Hola\n";
	}
	~X()
	{
		cout<<"bye\n";
	}
	
};
ostream& operator<<(ostream& os, const X& obj)//esta funcion nos deja imprimir el objeto de x
{
	os<<obj.p;
	return os;
}
int  main()
{
	A<int> a{614};
	a.show();
	A<string> b{"hola"};
	b.show();
	A<int *>c{new int{418}};//por el int * esto es un new
	c.print();
	A<X*>d{new X{}};
	d.print();//aqui llama a la funcion cout que sobreescribimos
}