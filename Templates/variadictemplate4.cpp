#include <iostream>
#include <string>
//libfmt implementa esto
using namespace std;

void internal_join(const string &aux)
{
    aux = aux + "";
}

template<typename ...args>
void internal_join(const string &aux, const string &sep, const args&...ag)
{
    aux = aux + sep + internal_join(ag...);
}

template<typename ...args>
string join(const string &sep, const string &m, const args &...ag)
{
    string aux;
    internal_join(aux,sep,ag...);
    return aux;
}

int main()
{
    string p = join(",","Juan","Jose","Maria","Claudia");
    cout << p << endl;
}