//especializacion de templates
#include<iostream>
#include<string>

using namespace std;
template <class T>
void Say_something(const T& x)
{
	cout<<x<<"\n";
}
template<>//esto se llama especializacion total
void Say_something <float>(const float& n)
{
	if(n==3.14f)
		cout<<"P\n";
	else
		cout<<"No p\n";
}
template <class T>
T next(const T& n);
template<>
int next <int>(const int& n)
{
	return n+1;
}
int main()
{
	Say_something<int>(6);
	Say_something<float>(3.14);
	Say_something<string>("hola");
	Say_something((float)8);
	cout<<next(6)<<"\n";
}