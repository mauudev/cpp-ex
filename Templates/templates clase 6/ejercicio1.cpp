#include <iostream>
using namespace std;
template<class U>
struct linked_list_node
{
	U elem;
	linked_list_node<U>*next;
};
template<class T>
class linked_list final //final significa que no se puede heredar
{
	linked_list_node<T>*first;
	linked_list_node<T>*last;
public:
	linked_list():first{nullptr},last{nullptr}
	{}
	void push_back(const T& obj)
	{
		/*auto*/
		linked_list_node<T>* nn=new linked_list_node<T>{obj,nullptr};
		if (first==nullptr)
		{
			first=last=nn;
			return ;
		}
		last->next=nn;
		last=nn;
	}
	void iterate(void(*f)(T))
	{
		auto aux=first;
		while(aux)
		{
			f(aux->elem);
			aux=aux->next;
		}
	}
	~linked_list()
	{
		auto aux=first;
		while(aux)
		{
			auto next=aux->next;
			delete aux;
			aux=next;
		}
	}

};
template<typename X>
void show(X x)
{
	cout<<x<<"\n";
}
void show_int(int x)
{
	cout<<x<<"\n";
}
int main()
{
	linked_list<int>x;//hay que especificar en el caso de que sea un template de una clase <int>
	x.push_back(16);
	x.push_back(10);
	x.push_back(8);
	x.push_back(20);
	x.push_back(10);
	x.iterate(show_int);
	x.iterate(show<int>);

	linked_list<string>y;//hay que especificar en el caso de que sea un template de una clase <int>
	y.push_back("hey");
	y.push_back("10");
	y.push_back("8");
	y.push_back("20");
	y.push_back("hola");
	y.iterate(show<string>);

}