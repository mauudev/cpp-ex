#include<string>
#include<iostream>

//SFINAE cuando busca funciones que se acomoden cuando ocurren errores, lo hace hasta el final
using namespace std;

template<typename A, typename B, typename C>
struct Test
{
	A a; B b; C c;
	void show()const
	{
		cout << a << " " << b << " " << c ;
	} 
};
template<typename X>
struct Test<int,X,int>
{
	int x; X y; int z;
	void show()const
	{
		cout << (x + z) << y << "\n";
	}
};
template<>
struct Test<int,int,int>
{
	string s;
	void print()const
	{
		cout << s << "\n";
	}
};
int main()
{
	Test<int,int,string> test1 {1,6,"h"};
	test1.show();

	Test<int,string,int>  test2 {6,"h",10};
	test2.show();

	Test<int,float,int>  test3 {6,3.14,10};
	test3.show();

	Test<int,int,int>  test4 {"hola mundo"};
	test4.print();
}