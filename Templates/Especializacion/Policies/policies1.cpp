#include<string>
#include<iostream>


using namespace std;

template<typename HablarPolicy>
struct Primate
{
	HablarPolicy hablarPolicy;
	void hablar()const
	{
		hablarPolicy.hablar();
	}
};

struct HumanoHablarPolicy
{
	void hablar()const
	{
		cout << "BLA BLA BLA\n";
	}
};

struct MonoHablarPolicy
{
	void hablar()const
	{
		cout << "UH UH UH\n";
	}
};

using Humano = Primate<HumanoHablarPolicy>;//es un alias que implementa la policy hablarhumano
using Mono = Primate<MonoHablarPolicy>;

int main()
{
	Humano h;
	h.hablar();

	Mono m;
	m.hablar();
}
