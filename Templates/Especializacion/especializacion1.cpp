#include<string>
#include<iostream>


using namespace std;
// LOS STRUCTS SON TOTALMENTE DIFERENTES EL COMPILADOR SOLO BUSCA LA FUNCION QUE SE ACOMODA  AL QUE NECESITA
template<typename T>
struct A
{
	T n;
	void show()const
	{
		cout << n << "\n";
	}
};

template<typename T>
struct A<T*>
{
	T *n;
	~A(){delete n;}
	void print()const
	{
		cout << *n << "\n";
	}
};

struct X
{
	int p = 8;
	X(){cout << "Hola X \n";}
	~X(){cout << "Chau X \n";}
};

ostream& operator<<(ostream& os, const X& obj)
{
	os << obj.p;
	return os;
}

int main()
{
	A<int> a {614};
	a.show();
	A<string> b {"hola"};
	b.show();
	A<int*> c {new int{418}};
	c.print();
	A<X*> d {new X{}};
	d.print();
}