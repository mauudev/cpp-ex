#include<string>
#include<iostream>

using namespace std;

template<class T>
struct eq
{
    bool are_equal(const T& a, const T& b)const
    {
        return a == b;
    }
};


template<typename T>//esto es tipo parametrizado por eso ponemos typename
struct eq<T*>
{
    bool are_equal(T* a, T* b)const
    {
        eq<T> cmp;
        return cmp.are_equal(*a,*b);
    }
};

struct CI
{
    string n;
    string city;
};

struct P
{
    int n;
    ~P(){puts("Bye P");}
};

template<>//esto es especialziacion total porq P ya esta definido
struct eq<P>
{
    bool are_equal(const P& a, const P& b)const
    {
        return a.n == b.n;
    }
};

struct CIeq
{
    bool are_equal(const CI &x, const CI &y) const
    {
        return x.n == y.n && x.city == y.city;
    }
};

template <typename T>
struct deleter
{
    void free(const T&){}
};

template <typename T>//esta es una especializacion para manejar obj de tipo T*
struct deleter<T*>
{
    void free(T* obj)
    {
        delete obj;
    }
};

template<typename T,typename EQ = eq<T>, typename DELETER = deleter<T>>//para eliminar los tipo puntero
class array_list
{
    size_t count;
    size_t cap;
    T* items;

public:
    array_list(size_t cap = 4):count{0},cap{cap},items{new T[cap]}{}
    ~array_list()
    {
        DELETER d;
        for(auto i = 0U; i < count; i++)
        {
            d.free(items[i]);
        }
        delete [] items;
    }

    void push_back(const T& obj)
    {
        if(count == cap) resize();
        items[count] = obj;
        count ++;
    }

private:
    void resize()
    {
        auto ncap = cap * 2;
        auto nitems = new T[ncap];
        for(auto i = 0; i < count; i++)
            nitems[i] = items[i];
        cap = ncap;
        delete [] items;
        items = nitems;
    }
    
public:
    int index_of(const T& obj)const
    {
        EQ eq;
        for(auto i = 0; i < count; i++)
        {
            if(eq.are_equal(obj,items[i]))
                return i;
        }
        return -1;
    }
};

int main()
{
    // array_list<int> x;
    // array_list<string> y;
    // x.push_back(15);
    // x.push_back(18);
    // x.push_back(25);
    // x.push_back(55);
    // y.push_back("hola");
    // y.push_back("mundo");
    // x.push_back(14);

    // auto pos = y.index_of("mundo");

    // array_list<CI, CIeq> q;
    // q.push_back(CI{"4433554","LP"});
    // q.push_back(CI{"4444444","PT"});
    // q.push_back(CI{"5555555","CB"});

    // auto idx = q.index_of(CI{"5555555","CB"});
    // if(idx != -1)
    //     cout << "POS: " << idx << "\n";
    // else cout << "NOT FOUND !" << "\n";

    // array_list<int*> x;
    // x.push_back(new int{6});
    // x.push_back(new int{14});
    // x.push_back(new int{18});

    // int c = 14;
    // auto idd = x.index_of(&c);
    // cout << idd << "\n";
    array_list<P*> ps;
    ps.push_back(new P{1});
    ps.push_back(new P{2});
    P d{1};
    auto ide = ps.index_of(&d);
    cout << ide << "\n";
}