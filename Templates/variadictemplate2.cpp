#include <iostream>
#include <string>
//using namespace std;
using namespace std;

void show(){}

template<typename T>
void show_p(const T &y)
{
    cout << y << endl;
}

template<typename T, typename ...ARGS>
void show_p(const T &x, const ARGS &...args)
{   
    cout << x << endl;
    show_p(args...);
}

template<typename ...ARGS>
void show(const ARGS...args)
{
    show_p(args...);
}

int main()
{
    //show(6,4,true,"hello",3.1416);
    show("hola","mundo",26,"bye");
}

