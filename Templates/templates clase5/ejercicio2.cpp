//templates
//generic programing
//ESTO SE PUEDE HACE DE OTRA FORMA
#include <iostream>
using namespace std;
/*int sum (int a , int b)
{
	return a+b;
}
double sum(double a,double b)
{
	return a+b;
}*/
template<class T,class U>//template<typename T>//estas lineas son lo mismo determinan
auto sum (const T &a,const U &b)
{
	return a+b;
}

int main()
{
	auto p=sum(8,10);
	cout<<p<<"\n";
	double q= sum(3.14,8.16);
	cout<<q<<"\n";
	auto m=sum(string{"hola"},string{"mundo"});
	auto z=sum<string,string>("hola","mundo");
	cout<<m<<"\n";
	cout<<z<<"\n";

}