#include <iostream>
#include <string>

using namespace std;

struct Heavy
{
    char *x;
    Heavy():x{new char[10'000'000]}
    {

    }

    ~Heavy(){delete [] x;}
    Heavy(const Heavy& src):x{new char[10'000'000]}
    {
        for(int i = 0; i < 10'000'000; i++)
        {
            x[i] = src.x[i];
        }
    }

    Heavy& operator=(const Heavy& src)
    {
        delete [] x;
        x = new char[10'000'000];
        for(int i = 0; i < 10'000'000; i++)
        {
            x[i] = src.x[i];
        }
        return *this;
    }

    Heavy(Heavy&& src):x{src.x}
    {
        src.x = nullptr;
    }

    Heavy& operator=(Heavy&& src)
    {
        delete [] x;
        x = src.x;
        src.x = nullptr;
        return *this;
    }
};
template<class T>
void my_swap(T& a, T& b)
{
    // T aux = a; 
    // a = b;
    // b = aux;

    T aux = move(a); 
    a = move(b);
    b = move(aux);
}
int main()
{

    Heavy obj1;
    Heavy obj2;

    for(int i = 0; i < 10'000'000; i++)
    {
        my_swap(obj1,obj2);
    }
}