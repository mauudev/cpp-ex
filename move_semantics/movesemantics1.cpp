#include <iostream>
#include <string>

using namespace std;

struct MyObj
{
    char *p;
    MyObj():p{new char[1000000]}
    {
        cout << "Create !" << endl;
    }

    MyObj(const MyObj& src):p(new char[1000000])
    {
        for(int i = 0; i < 1000000; i++)
        {
            p[i] = src.p[i];
        }
        cout << "Copied !" << endl;
    }

    MyObj(MyObj&& src):p{src.p}//esto es rvalue reference
    {
        src.p = nullptr;
        cout << "Moved !" << endl;
    }

    ~MyObj()
    {
        delete [] p;
        cout << "Delete !" << endl;
    }


    MyObj& operator=(const MyObj& src)
    {
        delete [] p;
        p = new char[1000000];
        for(int i = 0; i < 1000000; i++)
        {
            p[i] = src.p[i];
        }
        cout << "=ed" << endl;
        return *this;
    }
};

MyObj create()
{
    return MyObj{};
}

int main()
{
    // MyObj obj;
    // obj = create();

    MyObj obj1 = create();
    MyObj obj2 = std::move(obj1); //llama al contructor move de obj
}