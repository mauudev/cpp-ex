#ifndef LINE_H
#define LINE_H
#include "shape.h"
template<typename RENDERER>
class line:public shape<RENDERER>
{
public:
    line()
    :shape<RENDERER>("line")
    {}
    ~line()
    {}
    
    void  paint(RENDERER& p_paint) const override
    {
        
    }
};

#endif