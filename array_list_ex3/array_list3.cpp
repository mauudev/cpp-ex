#include <iostream>
#include <string>

using namespace std;

template<typename T>
class array_list
{
    T* items;
    size_t cap;
    size_t count;
private:
    struct al_it //array list iterator
    {
        T* item_pos;

        bool operator!=(const al_it& src) const
        {
            return item_pos != src.item_pos;
        }

        al_it& operator++()//preincremento
        {
            item_pos++;
            return *this;
        }
        al_it operator++(int)// no retorna referencia porq crea una copia (es mas lento)
        {
            al_it n{item_pos};
            item_pos++;
            return n;
        }

        T& operator*()
        {
            return *item_pos;
        }

        const T& operator*()const
        {
            return *item_pos;
        }

        T& operator[](size_t index)
        {
            return items[index];
        }

        const T& operator[](size_t index)const
        {
            return items[index];
        }
    };
public:
    array_list(size_t cap = 4):items{new T[cap]},cap{cap},count{0}
    {

    }
    ~array_list()
    {
        delete [] items;
    }
    template<typename U>
    void push_back(U&& item)
    {
        resize();
        items[count++] = forward<U>(item);
    }

    void resize()
    {
        if(cap != count) return ;
        size_t ncap = cap * 2;
        T* aux = new T[ncap];
        for(size_t i = 0U; i < count; i++)
        {
            aux[i] = move(items[i]); //mucho mas barato porq no se llama al constructor copia 
        }
        delete [] items;
        items = aux;
        cap = ncap;
    }

    // template<typename U>
    // void emplace_back(U&& x)//crea el punto en la estructura
    // {
    //     resize();
    //     items[count++] = T{forward<U>(x)};
    // }

    template<typename ...ARGS>
    void emplace_back(ARGS&& ...args)
    {
        resize();
        items[count++] = T{forward<ARGS>(args)...};//forward(param1), forward(param2)... los puntos hacen esto, si se pone dentro hace forward(param1,param2,param3..) y esto no funciona
    }
    using iterator = al_it;
    iterator begin()
    {
        return iterator{items};
    }

    iterator end()
    {
        return iterator{items + count};
    }

public:
    using type = T;//con esto decimos al comp q es un template no un tipo especifico
};

struct Point
{
    int x,y;
};

template<typename T>
void info(const T& x)
{
    typename T::type aux; // para q el compilador entienda  q type no es una variable de T y T es un template, un tipo de var
    cout << aux << endl;
}
int main()
{
    array_list<string> s;
    s.push_back("Hello");
    s.push_back("mundo");
    s.push_back("today");
    s.push_back("is");
    s.emplace_back("friday!");
    for(auto& i : s)
        cout << i << endl;
    for(auto i = s.begin(); i != s.end(); i++)
        cout << *i << endl;
    array_list<Point> pts;
    pts.push_back(Point{6,4});
    pts.push_back(Point{6,28});
    pts.push_back(Point{1001,4});
    pts.emplace_back(125,5);
    for(auto& p : pts)
        cout << p.x << "," << p.y << endl;

    array_list<string> q;
    info(q);
    array_list<int> r;
    info(r);
}