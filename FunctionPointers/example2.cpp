#include <cstring>
#include <iostream>
#include <cstdlib>

using namespace std;

int cmp(const void *a, const void *b)
{
  auto aa = static_cast<const int*>(a);
  auto bb = static_cast<const int*>(b);
  return *aa - *bb;
}

int cmps(const char* a, const char* b)
{
  auto aa = (const char**) (a);
  auto bb = (const char**) (b);
  return strcmp(*aa, *bb);

}

int main()
{
  int m[] = {6,9,2,1,0,8};
  qsort(m,6,sizeof(int),cmp);
  for(int n : m) cout << n << "\n";
  const char* ss[] = {"marzo","abril","enero"};
  qsort(ss, 3, sizeof(const char**), cmps);
  for(auto i : ss) cout << i << "\n";
}