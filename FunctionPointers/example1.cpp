#include <string>
#include <iostream>

using namespace std;
/*
  VARIABLES QUE ALMACENAN UNA DIRECCION DE MEMORIA
  EN LUGAR DE APUNTAR A UN DATO, APUNTAN A UNA FUNCION
  LA SINTAXIS DE LO PUNTEROS A FUNCIONES ESPECIFICAN LA SIGNATURA DE LA FUNCION APUNTADA
*/

int sum(int a, int b)
{
  return a + b;
}

int sub(int x, int y)
{
  return x - y;
}

int main()
{
  int (*q)(int, int);
  q = sum;
  cout << q(18,17) << "\n";
  q = sub;
  cout << q(25,4) << "\n";
}