#include <cstring>
#include <iostream>
#include <cstdlib>

using namespace std;

void print_progress(int p, int c)
{
  cout << p << " de " << c << "\n";
}

typeof void (*ProgressPtr)(int, int);
void f(ProgressPtr) (*pp)(int pos, int cant))
{
  for(int i = 0; i < 1000000; i++)
  {
    if(i % 2000000)
    {
      pp(1,2000000);
    }
  }
}
int main()
{
  f(print_progress);
} 