#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <exception>
#include <cctype>

using namespace std;

struct empieza_con
{
    char c;
    bool operator()(const string &s)const
    {
        return tolower(s[0]) == c;
    }
};

struct word_occurrence
{
    int count;
    string word;
    word_occurrence* next;
};

struct word_info
{
    
};
class text_store
{
    char *text;
    vector<string> m_tokens;
    word_occurrence *first;
    word_occurrence *last;

public:

    text_store()
    :first{nullptr},last{nullptr}
    {

    }

    ~text_store()
    {
        delete [] text;
    }

    void load_from_string(const string &str)
    {
        this->text = new char[str.length() + 1];
        strcpy(this->text, str.c_str());
        splitTokens(this->text);
    }

    size_t getVectorSize()
    {
        return m_tokens.size();
    }

    bool wordExist(const string &word)
    {
        auto aux = first;
        while(aux)
        {
            if(aux->word == word)
            {
                aux->count++;
                return true;
            }
            aux = aux->next;
        }
        return false;
    }

    void push_back_word(const string &word)
    {
        if(first == nullptr)
        {    
            word_occurrence *nodo = new word_occurrence{1,word,nullptr};
            first = last = nodo;
            return ;
        }
        if(wordExist(word) == false)
        {
            word_occurrence *nodo = new word_occurrence{1,word,nullptr};
            last->next = nodo;
            last = nodo;
        }
    }

    void iterateWord()
    {
        auto aux = first;
        while(aux != nullptr)
        {
            cout << aux->word << aux->count << endl;
            aux = aux->next; 
        }
    }

    void splitTokens(char *text)
    {
        string aux_word;
        char *pch;
        pch = strtok(text, " ’.',");
        while(pch != NULL)
        {
            aux_word = pch;
            push_back_word(aux_word);
            m_tokens.push_back(pch);
            pch = strtok(NULL, " ’.',");
        }
    }

    int get_word_count()
    {
        return getVectorSize();
    }

    template <typename PRED>
    int counter(vector<string> vec, size_t n, PRED pred)
    {
        int count = 0;
        for(auto i = 0U; i < n; i++)
        {   
            if(pred(vec.at(i))) count ++;
        }
        return count;
    }

	int get_word_count_by_first_letter(char letter)
	{
  //       int count = 0;
		// for(size_t i = 0U; i < getVectorSize(); i++)
  //       {
  //           string token = m_tokens.at(i);
  //           if(token[0] == letter || token[0] == toupper(letter))
  //               count ++;
  //       }
  //       return count;
        int count = 0;
        empieza_con ec{letter};
        count += counter(m_tokens, getVectorSize(), ec);
        return count;
	}

    template<typename PROC>
    void iterate_word_occurrences(const PROC& p)
    {
        auto aux = first;
        while(aux != nullptr)
        {
            p(*aux);
            aux = aux->next;
        }
    }
};

ostream& operator<<(ostream& os, const word_occurrence& c)
{
    return os << c.word + " (" + to_string(c.count) + ")";
}

const string text = "Doom’s success lay not only with its addicting gameplay and technical innovations, such as its introduction of a portable game engine, but also in its innovative digital distribution. Note the prominent 'shareware' advertisement on the top front of the game packaging. This groundbreaking game earned a spot in the inaugural class of the World Video Game Hall of Fame.";

bool test0()
{
    text_store ts;
    ts.load_from_string(text);
    return ts.get_word_count() == 60;
}

bool test1()
{
    text_store ts;
    ts.load_from_string(text);
    return ts.get_word_count_by_first_letter('d') == 3;
}

bool test2()
{
    text_store ts;
    ts.load_from_string("hello hello bye hello bye");
    size_t hello_count = 0, bye_count = 0;
    ts.iterate_word_occurrences([&](const word_occurrence& c)
    {
        if (c.word == "hello")
            hello_count = c.count;

        if (c.word == "bye")
            bye_count = c.count;
    });

    return hello_count == 3 && bye_count == 2;
}

bool test3()
{
    text_store ts;
    ts.load_from_string("binary world world");

    stringstream ss;
    ts.iterate_word_occurrences([&](const word_occurrence& c)
    {
        ss << c << "; ";
    });

    return ss.str() == "binary (1); world (2); ";
}

// bool test4()
// {
//     text_store ts;
//     ts.load_from_string(text);

//     const word_info& wloc = ts.get_word_info("its");

//     stringstream ss;
//     ss << wloc;

//     return ss.str() == "its (33, 91, 147)";
// }

// bool test5()
// {
//     try
//     {
//         text_store ts;
//         ts.load_from_string(text);

//         const word_info& wloc = ts.get_word_info("brisa mistica");

//         cout << wloc << "\n";
//         return false;
//     }
//     catch (const std::exception& ex)
//     {
//         return string{ ex.what() } == "'brisa mistica' not found";
//     }
// }

// bool test6()
// {
//     text_store ts;
//     ts.load_from_string(text);

//     size_t count = 0, total_count = 0;
//     ts.iterate_all_words_by_first_letter('t', [&](const word_info& w)
//     {
//         auto& word = w.word;
//         total_count++;
//         if (word == "technical" || word == "the" || word == "This" || word == "top")
//             count++;
//     });

//     return count == 4 && total_count == 4;
// }

// bool test7()
// {
//     text_store ts;
//     ts.load_from_string(text);

//     return ts.get_most_occurring_word().word == "the";
// }
// /*
// bool test8()
// {
//     text_store ts;
//     ts.load_from_file("vulgata.txt");

//     return ts.get_word_count() == 702484 &&
//            ts.get_word_count_by_first_letter('a') == 4260 &&
//            ts.get_most_occurring_word().word == "et" &&
//            ts.get_most_occurring_word().count == 51614 &&
//            ts.get_word_info("maria").locations.back() == 3736761;
// }

// bool test9()
// {
//     return test8();
// }
// */

int main()
{
    size_t score = 0;

    auto t0 = clock();

    score += test0();
    score += test1();
    score += test2();
    score += test3();
    score += test4();
    // score += test5();
    // score += test6();
    // score += test7();
    // score += test8();
    // score += test9();

    auto t1 = clock();

    cout << "FINAL SCORE: " << score << " / 10\n";
    cout << "EXECUTION TIME: " << ((t1 - t0) / 1000) << "\n ";

    getchar();

    return 0;
}