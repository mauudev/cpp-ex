#include <iostream>
#include <cstring>
using namespace std;

char* strsort(const char* s){
	size_t len = strlen(s);
	auto  nn = new char[len + 1];
	memcpy(nn,s,len+1);	
	char* res = new char[len+1];
	bool permutar = true;
	size_t i = 0;
	while(permutar){
		permutar = false;
		i++;
		for(size_t j = 0; j < (len - i); j++){
			if(nn[j] > nn[j+1]){
				permutar = true;
				char temp = nn[j];
				nn[j] = nn[j+1];
				nn[j+1] = temp;
			}
		}
	}
	return nn;
}

char* delete_repeats(const char* s){
	size_t len = strlen(s);
	size_t i = 1;
	size_t j = 0;
	auto nn = new char[len+1];
	s = strsort(s);
	memcpy(nn,s,len+1);
	auto res = new char[len+1];
	res[j] = nn[j]; 
	while(i <= len){
		char character = nn[i];
		if(character == res[j]){

		}else{
			j++;
			res[j] = character;
		}
		i++;
	}
	return res;
}
void show(char* s){
	size_t len = strlen(s);
	for(size_t i = 0; i < len; i++)
		cout << s[i] << ",";
	cout << endl;
}

int main(){
	int s[] = {4,3,6,3,3,7,1};
	char s1[] = "aaaaggfffccbbbbbhhrreegff";
	char* res = delete_repeats(s1);
	cout << res << endl;
	//show(res);
	//longest_str(s1);
}