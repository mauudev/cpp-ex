#include <string>
#include <iostream>

using namespace std;

std::string remove_spaces(std::string x, int index, std::string res)
{
    if(index < x.length())
    {
      if(x.at(index) == ' ') return remove_spaces(x,index+1,res);
      else
      {
        res += x.at(index);
        return remove_spaces(x,index+1,res);
      }
    }
    return res;
}

std::string no_space(std::string x)
{
    string res = "";
    return remove_spaces(x,0,res);
}

int main()
{
    string s = "hola esta es una pruebita !";
    string str = no_space(s);
    cout << s << endl;
    cout << str << endl;
}