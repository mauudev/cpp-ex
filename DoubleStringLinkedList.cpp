#include<cstring>
#include<iostream>
using namespace std;

class DoubleStringLinkedList{
	struct Node{

	public:
		size_t len;
		char* s;
		Node* next;
		Node* previous;
	};
	Node* first;
	Node* last;
	size_t count;

	public:
	DoubleStringLinkedList():first{nullptr},last{nullptr},count{0}{}

	~DoubleStringLinkedList(){
		Node* node = first;
		while(node != nullptr){
			Node* next = node->next;
			delete [] node->s;
			delete node;
			node = next;
			cout << "Bye" << endl;
		} 
	}

	DoubleStringLinkedList(const DoubleStringLinkedList&) = delete;

	Node* getFirst(){return this->first;}
	Node* getLast(){return this->last;}

	void clean_list(){
		Node* node = first;
		while(node != nullptr){
			Node* next = node->next;
			delete [] node->s;
			delete node;
			node = next;
		} 
	}
	void operator=(const DoubleStringLinkedList&) = delete;

	void push_back(const char* p){
		auto len = strlen(p);
		auto nn = new char[len+1];
		memcpy(nn,p,len+1);
		count++;
		auto node = new Node{len,nn,nullptr,nullptr};
		if(first == nullptr){
			first = last = node; 
			return ;
		}
		node->previous = last;
		last->next = node;
		last = node;
	}

	void push_front(const char* p){
		auto len = strlen(p);
		auto nn = new char[len+1];
		memcpy(nn,p,len+1);
		count ++;
		auto node = new Node{len,nn,nullptr,nullptr};
		if(first == nullptr){
			first = last = node;
			return ;
		}
		first->previous = node;
		node->next = first;
		first = node;
	}

	void erase(const char* p){
		auto actual = first;
		erase(p,actual,count,1);
	}
	void erase(const char* p, Node* actual, size_t count, size_t walker){
		if(strcmp(p,actual->s) == 0 && walker == 1){//estamos en el first
			first = actual->next;
			first->previous = nullptr;
			delete [] actual->s;
			delete actual;
			count --;
			//return ;
		}
		if(strcmp(p,actual->s) == 0 && walker == count){//estamos en el last
			last = actual->previous;
			actual->previous->next = nullptr;
			delete [] actual->s;
			delete actual;
			count --;
			//return ;
		}
		if(strcmp(p,actual->s) == 0 && walker != count && walker != 1){//otherwise
			actual->previous->next = actual->next;
			actual->next->previous = actual->previous;
			delete [] actual->s;
			delete actual;
			count --;
			//return ;
		}else erase(p,actual->next,count,walker+1);
	}
	void show()const{
		Node* aux = first;
		while(aux){//while(aux==nullptr)
			cout << aux->s << endl;
			aux = aux->next;
		}
	}
};
/*int main(){
	DoubleStringLinkedList s;
	s.push_back("hello");
	s.push_back("world");
	s.push_back("c++");
	s.push_back("rules");
	s.show();
	//s.push_front("x");
	cout << "-------------------" << endl;
	s.erase("c++");
	s.show();
}*/

