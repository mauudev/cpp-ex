#include <iostream>
using std::cout;

namespace a
{
  namespace b
  {
    namespace c
    {
      void d()
      {
        cout << "Nested !\n";
      }
      void e()
      {
        cout << "EEE\n";
      }
    }
  }
}

namespace x::y::z
{
  void w()
  {
    cout << "Yeah !!\n";
  }
}

using namespace a::b::c;
int main()
{
  a::b::c::d();
  e();
  x::y::z::w();
}