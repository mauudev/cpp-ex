#include <iostream>

using std::cout;

//sirve para eliminar ambiguedades

namespace funcs
{
  void f()
  {
    cout << "Hello !\n";
  }

  void g()
  {
    cout << "2018\n";
  }
}
void g()
{
  cout << "bye\n";
}

using namespace funcs;  

int main()
{
  funcs::f();
  funcs::g();
       ::g();
}