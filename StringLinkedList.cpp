#include<cstring>
#include<iostream>
using namespace std;
class StringLinkedList{
	struct Node{
	public://como esta es public la clase padre puede acceder a los atributos, si fuera private NO y Node puede acceder a los privados de StringLinkedList
		size_t len;
		char* s;
		Node* next;

	};//esta clase puede acceder a los elementos privados de la clase StringLinkedList
	Node* first;
	Node* last;
	size_t count;
	public:
	StringLinkedList():first{nullptr},last{nullptr},count{0}{

	}
	~StringLinkedList(){
		Node* node = first;
		while(node != nullptr){
			Node* next = node->next;
			delete [] node->s;
			delete node;
			node = next;
			//cout << "Bye" << endl;
		} 
		count = 0;
	}
	void clean_list(){
		Node* node = first;
		while(node != nullptr){
			Node* next = node->next;
			delete [] node->s;
			delete node;
			node = next;
			//cout << "Bye" << endl;
		} 
		count = 0;
	}
	/*Str(const Str& src):len{src.len}{
		if(len < MAXSTR){
			memcpy(sso,src.sso,len+1);
			return ;
		}
		s = new char[len+1];
		memcpy(s,src.s,len+1);
	}*/
	Node* getFirst(){return this->first;}
	Node* getLast(){return this->last;}
	StringLinkedList& operator=(const StringLinkedList& src) {//cual es la diff?
		if(this == &src) return *this;//cual es la diff?
		this->~StringLinkedList();
		auto actual = src.first;
		
		while(actual){
			this->push_back(actual->s);
			actual = actual->next;
		}
	}	
	
	void push_back(const char* p){//como no sabemos la naturaleza de este char , hacemos una copia local
		auto len = strlen(p);
		auto nn = new char[len+1];
		memcpy(nn,p,len+1);
		auto node = new Node{len,nn,nullptr};//new siempre devuelve un puntero
		count++;
		if(first == nullptr){
			first = last = node;
			return ;
		}
		last->next = node;
		last = node;
	}

	void push_front(const char* p){
		auto len = strlen(p);
		auto nn = new char[len+1];
		memcpy(nn,p,len+1);
		auto node = new Node{len,nn,nullptr};
		count++;
		if(first == nullptr){
			first = last = node;
			return ;
		}
		node->next = first;
		first = node;
	}

	void erase(const char* p){
		auto actual = first;	
		erase(p,actual,count,1);
	}

	void erase(const char* p, Node* actual, size_t count, size_t walker){
		if(strcmp(actual->s,p) == 0 && walker == 1){//estamos en el first
			//cout << "estamos en el principio" << endl;
			first = actual->next;
			delete [] actual->s;
			delete actual;
			count--;
			return ;//si no agrego esto me sale segmentation fault
		}
		if(strcmp(actual->s,p) == 0 && walker == count){//estamos el last
			//cout << "estamos en el last" << endl;
			auto before = get_node_by_pos(walker-1);
			last = before;
			delete [] actual->s;
			delete actual;//aqui debemos hacer last->next = nullptr???
			count--;
			return ;//si no agrego esto me sale segmentation fault
		}
		if(strcmp(actual->s,p) == 0 && walker != count && walker != 1){//otherwise
			//cout << "estamos en el otherwise";
			auto before = get_node_by_pos(walker-1);
			before->next = actual->next;
			delete [] actual->s;
			delete actual;
			actual->next = nullptr;
			count--;
			return ;//si no agrego esto me sale segmentation fault
		}else erase(p,actual->next,count,walker+1);
	}

	Node* get_node_by_pos(size_t pos){
		size_t i = 1;
		auto actual = first;
		if(pos == 0) return actual;
		if(pos == i) return actual;
		if(pos >= count) return last;
		while(i < pos){
			actual = actual->next;
			i++;
		}
		return actual;
	}

	void show() const {
		Node* aux = first;
		while(aux){//while(aux==nullptr)
			cout << aux->s <<  endl;
			aux = aux->next;
		}
		cout << "Count: "<< count  << endl;
	}
};

int main(){
	StringLinkedList s,t;
	s.push_back("hello");
	s.push_back("world");
	s.push_back("c++");
	s.push_back("rules");
	s.show();
	cout << "--------------" << endl;
	t.push_back("hola");
	t.push_back("mundo");
	t.push_back("java");
	t.push_back("la hace");
	t.push_back("yeah");
	t.show();
	/*cout << "--------------" << endl;
	cout << "Haciendo la copia" << endl;
	cout << "--------------" << endl;
	s = t;
	s.show();*/
	//cout << s.get_node_by_pos(0)->s << endl;
	s.erase("rules");
	s.show();
	//s.push_back("x");
}
/*
	IMPLEMENTAR:

	- Volver la lista a doblemente elanzada
	- size_t erase(const char* p);
	- constructor copia:
	operator=
*/