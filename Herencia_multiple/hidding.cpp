#include <iostream>

using namespace std;

class M{

public:
	void p (){puts("M");}
};

class N{

public:
	void p(){puts("N");}
};

class MN : public M, public N{

public:
	// void p(){ //ESTO ES EL HIDDING, SOLO IMPORTA ESTE METODO NO LOS QUE SE HEREDAN
	// 	puts("MN");
	// }
	void q(){
		M::p();
		N::p();
	}
};

int main(){
	M m;
	N n;
	MN mn;

	m.p();
	n.p();
	
	mn.q();
}