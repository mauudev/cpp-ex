#include <iostream>
#include <string>

using namespace std;

struct X {
	X(){puts("I am X !");}
};

struct Y
{
	int n;
	Y(int n):n{n}{puts("I am Y !");}
};

struct  Z : X,Y
{
	Z():Y{1240}	{}
};

int main(){
	Z z;
	cout << z.n << endl;
}