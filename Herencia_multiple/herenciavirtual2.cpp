#include <iostream>
#include <string>

using namespace std;

struct P
{
	virtual ~P(){}
	virtual void show() const = 0;
};

struct Q : P
{
	int n;
	Q(int n):n{n}{}
	void show ()const override
	{
		cout << n << "\n";
	}
	string to_string() const 
	{
		return std::to_string(n);
	}
};

struct R :P 
{
	string s;
	R(const string& s):s{s}{}

	void show() const override
	{
		cout << s << "\n";
	}
};

// void do_something(P *p) {
// 	p->show();
// 	Q *q = dynamic_cast<Q*>(p);
// 	if(q == nullptr)
// 		cout << "No casting \n";
// 	else
// 		cout << "TS" << q->to_string() << "\n";
// }

void do_something(P &p)
{
	p.show();
	try{

		Q &q = dynamic_cast<Q&>(p);
		cout << "TS" << q.to_string() << "\n";
	}catch(...){
		cout << "Bad cast error" << "\n";
	}
}

int main()
{
	P *q = new Q(114);
	P *c = new R("Hola");
	do_something(*c);
	do_something(*q);
}