#include <cstdio>
#include <iostream>

using namespace std;
struct X
{
	virtual void doSomething() = 0;	
};

struct Y : public virtual X
{
	void doSomething() override
	{
		puts("OOPS");
	}
};

struct Z : public virtual X{};


struct ZZZ: Y, Z
{
	void doSomething() override
	{
		puts("Hello from dev26 !");
	}
};

int main()
{
	// ZZZ z;
	// z.doSomething();

	X* x = new ZZZ();
	Y* y = new Y();
	Z* z = new ZZZ();

	ZZZ* z2 = new ZZZ();

	x->doSomething();
	y->doSomething();
	z->doSomething();

	z2->doSomething();
}