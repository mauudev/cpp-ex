#include <iostream>
#include <string>

using namespace std;

struct SeMueve{

	void mover(int pos){
		cout << "Me movi " << pos << " metros !" << endl;
 	}
};

struct Vuelo{
	void volar(int a){
		cout << "Vuelo " << a << " metros !" << endl;
	}
};

struct Come {
	void comer(int kg){
		cout << "Como " << kg << " kilos !" << endl;
	}
};

struct Vaca : SeMueve, Come{//todo es public en los structs
	void darCarne(int kg){
		cout << "Doy " << kg << " kilos !" << endl;
	}
};

struct Paloma : SeMueve, Come, Vuelo {

};

struct Helicoptero : Vuelo {

};

void alimentar(Come& c){
	c.comer(10);
}

int main (){
	Helicoptero h;
	Paloma p;
	Vaca v;
	h.volar(100);
	p.volar(100);
	p.mover(10);
	puts("***");
	alimentar(p);
	alimentar(v);
}















