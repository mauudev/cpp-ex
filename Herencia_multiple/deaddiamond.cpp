#include <iostream>

using namespace std;

struct R
{
	int x;
};

struct S1:R
{
	
};

struct S2:R
{
	
};

struct T:S1,S2
{
	
};

int main(){
	T p;

	p.S1::x = 14;
	p.S2::x = 25;
	cout << p.S1::x << endl;
	cout << p.S2::x << endl;
}