#include <iostream>
#include <string>

using namespace std;

class A{

public:
	int x;

	void m(){puts("Hello from A!");}
};

class B{

public:
	int y;

	void n(){puts("Hello from B!");}
};

class AB : public A, public B{

};

int main(){
	AB ab;
	ab.x = 20;
	ab.y = 90;

	cout << ab.x << endl;
	cout << ab.y << endl;

	ab.m();
	ab.n();
}