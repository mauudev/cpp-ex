#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

using namespace std;

template<typename T>
class StringBuilder 
{
	T* builder;
	size_t len;
	size_t memory_size;

public:
	StringBuilder(){
		memory_size = 5;
		builder = new T[memory_size];
		len = 0;
	}

	~StringBuilder(){delete [] builder;}

    void append(int a)
    {
        string s = std::to_string(a); 
        if(len < memory_size)
        {
            builder[len] = s;
            len++;
            return ;
        }
        T* aux = new T[memory_size];
        for(auto i = 0U; i < len; i++){
            aux[i] = builder[i];
        }
        memory_size = memory_size * 5;
        builder = new T[memory_size];
        for(auto i = 0U; i < len; i++){
            builder[i] = aux[i];
        }
        builder[len] = s; 
        len++;
        delete [] aux;
    }

	void append(string s)
	{
		if(len < memory_size)
		{
			builder[len] = s;
			len++;
			return ;
		}
		T* aux = new T[memory_size];
		for(auto i = 0U; i < len; i++){
			aux[i] = builder[i];
		}
		memory_size = memory_size * 5;
		builder = new T[memory_size];
		for(auto i = 0U; i < len; i++){
			builder[i] = aux[i];
		}
		builder[len] = s; 
		len++;
		delete [] aux;
	}

	size_t get_size()
	{
		return len;
	}

	void getChars()
	{
		for(auto i = 0U; i < len; i++)
		{
			cout << builder[i] ;
		}
	}

	void appendLine(char s = "")
	{

		//    builder->append('"\n"');
	}
};

int main()
{
      StringBuilder<string> sb;

      

      for (int i = 0; i < 1000; i++)

      {

            for (int j = 0; j < 1000; j++)

            {
            	  sb.append("(");

                  //sb.append(std::to_string(i));
            	  sb.append(i);
                  sb.append(", ");

                  //sb.append(std::to_string(j));
                  sb.append(j);
                  sb.append("); ");

            }

           

           // sb.appendLine("");

      }
      sb.getChars();
      //cout << sb.get_size() << endl;

      //cout << sb.getChars() << "\n";

}